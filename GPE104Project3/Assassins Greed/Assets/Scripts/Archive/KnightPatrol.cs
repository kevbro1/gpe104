﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightPatrol : MonoBehaviour
{
    private int randomSpot;
    public bool isOnPatrol = true;
    private float waitTime;
    public float startWaitTime = 3;
    public float walkSpeed;
    public List<Transform> moveSpots;
    // Start is called before the first frame update
    void Start()
    {
        randomSpot = Random.Range(0, moveSpots.Count);
        waitTime = startWaitTime;
        moveSpots = GameManager.instance.patrolSpots;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, moveSpots[randomSpot].position, walkSpeed * Time.deltaTime);

        if (Vector2.Distance(transform.position, moveSpots[randomSpot].position) < 0.1f)
        {
            if (waitTime <= 0)
            {
                randomSpot = Random.Range(0, moveSpots.Count);
                waitTime = startWaitTime;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }
    }
}
