﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script attached to mushrooms to grant player lives
public class Mushroom : MonoBehaviour
{
    private SoundManager sm; // Sound manager
    private void Start()
    {
        sm = GameManager.instance.soundManager; // Set sound manager variable
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        sm.SoundPickup(); // Play the pickup sound
        Destroy(this.gameObject); // Destroy current object on collision
        GameManager.instance.playerLives++; // Adds one life
    }
}
