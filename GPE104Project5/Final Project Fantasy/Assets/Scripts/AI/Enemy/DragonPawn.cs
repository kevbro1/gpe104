﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Dragon pawn
public class DragonPawn : EnemyPawn
{
    // Collision damage done to player
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject == GameManager.instance.player && canDamage == true)
        {
            playerData.currentHealth -= contactDamage; // Deals damage
            canDamage = false; // switch this to an ai state
            StartCoroutine("damagedelay");
        }
    }
    // Collision damage delay
    private IEnumerator damagedelay()
    {
        while (true)
        {
            yield return new WaitForSeconds(damageDelay);
            canDamage = true;
        }
    }

    // Dragon attack is launched from a child object.
    public void AttackPlayer()
    {
        // AI sees player and player is not being attacked by too many enemies
        this.gameObject.GetComponentInChildren<EnemyLaunch>().LaunchProjectile();

        // Plays dragon roar when dragon attacks
        GameManager.instance.soundManager.SoundDragonRoar();
    }
}
