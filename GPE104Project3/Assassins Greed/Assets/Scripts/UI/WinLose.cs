﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Win and Lose screen button functions
public class WinLose : MonoBehaviour
{ 
    // Change game state to menu
    public void MainMenu()
    {
        GameManager.instance.gameState = "menu"; 
    }

    // Quit game
    public void Quit()
    {
        GameManager.instance.GameQuit(); 
    }
}
