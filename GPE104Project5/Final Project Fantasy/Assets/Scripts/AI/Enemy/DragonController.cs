﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Controller for dragon enemy
public class DragonController : EnemyController
{
    public DragonPawn dragonPawn; // dragonPawn
    void Start()
    {
        enemyPawn = this.GetComponent<DragonPawn>(); // Make sure to set this to the correct enemy child pawn
        dragonPawn = this.GetComponent<DragonPawn>(); // Used for attack functions
        player = GameManager.instance.player; // Get player instance
        aiState = "wandering"; // Defaul aiState
    }
    // Dragon attack. Rotate towards player and launch fireballs
    public override void DoAttackPlayer()
    {
        dragonPawn.RotateTowardsPlayer();
        dragonPawn.AttackPlayer();
    }
}
