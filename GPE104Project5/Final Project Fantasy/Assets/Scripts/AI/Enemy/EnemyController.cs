﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Parent enemy controller
public abstract class EnemyController : AIController
{
    public EnemyPawn enemyPawn;
    public GameObject player;
    public void Update()
    {
        // AI State Machine
        if (aiState == "idle")
        {
            DoIdle(); // Idle
        }
        if (aiState == "wandering")
        {
            DoWandering();
            // if player is in range, then begin searching
            if(enemyPawn.PlayerDetected(player) == true)
            {
                aiState = "attackPlayer";
            }
            // If player is not detected but is in range
            if (enemyPawn.PlayerDetected(player) == false && GetComponentInChildren<PlayerInRange>().inRange == true)
            {
                aiState = "searching";
            }
        }
        if (aiState == "searching")
        {
            DoSearching();
            // If the player is detected attack the player
            if (enemyPawn.PlayerDetected(player) == true)
            {
                aiState = "attackPlayer";
            }
            // If the player is not detected and not in range go back to wandering
            if (enemyPawn.PlayerDetected(player) == false && GetComponentInChildren<PlayerInRange>().inRange == false)
            {
                aiState = "wandering"; // Once wandering is done, use wandering 
            }
        }
        if (aiState == "attackPlayer")
        {
            DoAttackPlayer();
            // If the player is not detected or in range, return to wandering
            if (enemyPawn.PlayerDetected(player) == false && GetComponentInChildren<PlayerInRange>().inRange == false)
            {
                aiState = "wandering"; // Once wandering is done, use wandering 
            }
            // If the player is not detected but in range, begin searching
            if (enemyPawn.PlayerDetected(player) == false && GetComponentInChildren<PlayerInRange>().inRange == true)
            {
                aiState = "searching"; // Once wandering is done, use wandering 
            }
        }
    }
    // Wander about aimlessly
    public void DoWandering()
    {
        enemyPawn.Wandering();
    }
    // Rotate around to find player
    public void DoSearching()
    {
        enemyPawn.Searching();
    }
    public virtual void DoAttackPlayer()
    {
        // Empty function that will be replaced by child
    }
}
