﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Updates the AI health bar
public class AIHealth : MonoBehaviour
{
    public AIPawn aiPawn;
    public Image aiHealth;

    private void Start()
    {
        aiPawn = this.gameObject.GetComponentInParent<AIPawn>();
    }
    // Update is called once per frame
    void Update()
    {
        aiHealth.fillAmount = aiPawn.enemyHealthPercent;
    }
}
