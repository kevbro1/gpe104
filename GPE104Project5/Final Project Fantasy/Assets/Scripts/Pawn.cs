﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Pawn parent class
public abstract class Pawn : MonoBehaviour
{
    // This can be a future way of dealing damage. Need another method to receive damage
    public virtual void GiveDamage(float damageAmount, GameObject target)
    {
        if (target == GameManager.instance.player)
        {
            target.GetComponent<PlayerData>().currentHealth -= damageAmount;
        }
        if (this.gameObject != CompareTag("Enemy") && target == CompareTag("Enemy"))
        {
            target.GetComponent<EnemyPawn>().currentHealth -= damageAmount;
        }
    }
}
