﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// AI Pawn parent class
public abstract class AIPawn : Pawn
{
    // Components
    public PlayerData playerData;
    public GameObject player;
    public Transform p;
    public AIController aiController;
    public WaveManager waveManager;

    // Basic stats for AI
    public float maxHealth;
    public float currentHealth;
    public float rangedPower;
    public float meleePower;
    public float deathDelay = 3;
    public float meleeForce;
    public float enemyHealthPercent;
    public float attackSpeed = 1;

    public virtual void Start()
    {
        currentHealth = maxHealth; // Reset health
    }
    public virtual void Update()
    {
        enemyHealthPercent = currentHealth / maxHealth; // Update enemy percent health for UI health bar
    }

    public void Idling()
    {
        // AI standing
    }
}
