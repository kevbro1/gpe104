﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script for coin pickup
public class Coin : MonoBehaviour
{
    private int coinUpperValue; // Upper value of the coin in random range
    private int coinLowerValue; // Lower value of the coin in random range
    private int coinValue; // Random value for the coin instance
    private void Start()
    {
        coinUpperValue = GameManager.instance.waveManager.coinUpperValue;
        coinLowerValue = GameManager.instance.waveManager.coinLowerValue;
        coinValue = Random.Range(coinLowerValue, coinUpperValue);
    }
    // Give the player the value of the coin if they collide with the coin
    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject == GameManager.instance.player) // Check if the other object is the player
        {
            GameManager.instance.soundManager.SoundPickup(); // Plays pickup sound when the player picks up a coin
            GameManager.instance.playerData.coins += coinValue; // Give the coin value to the player
            Destroy(this.gameObject); // Destroy the coin
        }
    }
}
