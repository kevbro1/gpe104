﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Script runs when player collides with an enemy or asteroid
public class PlayerDeath : MonoBehaviour
{
    private Transform p; // Transform component for player
    private GameObject player; // GameObject for player tag

    // Start is called before the first frame update
    private void Start()
    {
        player = GameObject.FindWithTag("Player"); // Set player to GameObject with "player" tag (Must tag the player in Unity)
        p = player.GetComponent<Transform>(); // Get transform component
    }
    void OnCollisionEnter2D(Collision2D collision) // On player collision
    {
        if (collision.gameObject.tag != "PowerUp") // Check that other.Gameobject is not a powerUp
        {
            PlayerReset(); // Call PlayerReset function if not a power up
        }
    }
    public void PlayerReset()
    {
        if (GameManager.instance.lives > 0)
        {
            GameManager.instance.lives--; // Subtract one life
            Vector3 myVector = new Vector3(0, 0, 0); // Resets player to center of play area.
            p.position = myVector; // player position set to myVector  
        }
    }
}
