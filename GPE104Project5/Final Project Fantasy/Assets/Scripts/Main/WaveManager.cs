﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Manages the wave system
public class WaveManager : MonoBehaviour
{
    // Wave Manager variables
    public string waveState = "beginWave";

    // Player
    public PlayerData playerData;
    public PlayerController playerController;
    public GameObject player;
    public Vector3 resetVector; // Location to reset player after each round

    // Canvas
    private GameObject victory;
    private GameObject defeat;
    private GameObject upgrade;

    // All enemy related variables
    public float maxEnemies = 10; // Maximum number of enemies in the round
    public float enemiesSpawned; // Number of enemies spawned in the round
    public float currentWave = 1;
    public float concurrentEnemies; // Number of enemies currently active
    public float maxConcurrentEnemies = 3; // Maximum number of enemies that can be active at once
    public float bodyCount; // Number of enemies killed in the current round
    public float percentEnemiesRemaining; // Percentage of enemies that are still active
    public float maxEnemiesIncrement; // Max enemies incremented after each wave
    public float concurrentIncrement; // Max concurrent enemies incremented after each wave
    private GameObject randomEnemy; // Random enemy object
    private Transform spawnPoint; // Transform component for spawnPoint
    public float spawnDelay = 2; // Delay to spawn enemies
    public List<GameObject> enemyList; // List of available enemies
    public List<Transform> enemySpawns; // List of spawn locations
    public GameObject[] enemyObjects; // Array of enemy objects, used to destroy after each round
    public GameObject[] enemyAttacks; // Array of enemy attacks, used to destroy after each round
    public List<Transform> wanderSpots; // List of spots for AI to wander between

    // Loot related variables
    public List<GameObject> lootList; // List of available loot
    public int coinUpperValue = 500; // Upper coin value for random
    public int coinLowerValue = 100; // Lower coin value for random

    void Start()
    {
        victory = GameManager.instance.victory;
        defeat = GameManager.instance.defeat;
        upgrade = GameManager.instance.upgrade;
        playerData = GameManager.instance.playerData;
        playerController = GameManager.instance.playerController;
        player = GameManager.instance.player;
        resetVector = new Vector3(0, 0, 0); // Resets player to center of play area.
    }

    void Update()
    {
        // Wave state machine is nested in the Game Manager. Game manager must be in "active" state for wave state machine to function
        // Start the wave
        if (GameManager.instance.gameState == "active" && waveState == "beginWave") 
        {
            DoBeginWave();
            // Transition to activeWave after pre-wave is done
            waveState = "activeWave";
        }
        // Game in the middle of the wave. Check for win or lose
        if (GameManager.instance.gameState == "active" && waveState == "activeWave") 
        {
            DoActiveWave();
            // Transition to victory after all enemies are killed
            if (bodyCount >= maxEnemies)
            {
                // Plays Woohoo sound effect when the wave is won, had to put it in the state machine to play at the right time
                GameManager.instance.soundManager.SoundWoohoo();
                waveState = "victory";
            } 
            // Transition to defeat if player's health is zero
            if (playerData.currentHealth <= 0)
            {
                // Plays Evil Laugh sound when you lose, had to put it in the state machine to play at the right time
                GameManager.instance.soundManager.SoundEvilLaugh();
                waveState = "defeat";
            }
        }
        // Load Defeat Canvas
        if (GameManager.instance.gameState == "active" && waveState == "defeat") 
        {
            DoDefeat();
        }
        // Load Victory Canvas
        if (GameManager.instance.gameState == "active" && waveState == "victory") 
        {
            DoVictory();
        }
        // Post wave calculations
        if (GameManager.instance.gameState == "active" && waveState == "postWave") 
        {
            DoPostWave();
        }
    }
    // Sets up new wave
    public void DoBeginWave()
    {
        // Close victory/defeat windows
        victory.SetActive(false);
        defeat.SetActive(false);
        upgrade.SetActive(false);

        // Enable all AI movement
        Time.timeScale = 1;

        // Enable player controller
        playerController.GetComponent<PlayerController>().enabled = true;

        // Create a player object if none exists
        if (GameManager.instance.player == null)
        {
            GameManager.instance.CreatePlayerObject(); // Create a new player object
            GameManager.instance.UpdatePlayer(); // Update the GameManager variables to new player object
        }
    }
    // Wave is active, spawn enemies
    public void DoActiveWave()
    {
        // Close victory/defeat windows
        victory.SetActive(false);
        defeat.SetActive(false);
        upgrade.SetActive(false);

        // Update the number of enemies remaining for HUD display
        UpdateEnemiesRemaining();

        // Start enemy spawning
        StartCoroutine("SpawnEnemyEvent"); // Begin coroutine for SpawnEnemyEvent

        // Enable all AI movement
        Time.timeScale = 1;

        // Enable player controller
        playerController.GetComponent<PlayerController>().enabled = true;
    }
    // Method run when player eliminates all enemies
    public void DoVictory()
    {
        // Destroy all enemies
        DestroyAllEnemies();

        // Victory Canvas
        victory.SetActive(true);

        // Enable all AI movement
        Time.timeScale = 0;

        // If bodyCount = maxEnemies then transition game to next wave
        if(bodyCount == maxEnemies)
        {
            StartCoroutine(NextWave()); // Begin next wave coroutine
            // Reset bodyCount, enemiesSpawned and concurrentEnemies to zero
            bodyCount = 0;
            enemiesSpawned = 0;
            concurrentEnemies = 0;
        }

        // Enable player controller
        playerController.GetComponent<PlayerController>().enabled = false;
    }
    // Method run when player dies
    public void DoDefeat()
    {
        // Destroy all enemies
        DestroyAllEnemies();

        // Defeat Canvas
        defeat.SetActive(true);

        // Enable all AI movement
        Time.timeScale = 0;

        // Enable player controller
        playerController.GetComponent<PlayerController>().enabled = false;
    }
    // Post wave method, resets variables and player location and launches upgrade window
    public void DoPostWave()
    {
        player.GetComponent<Transform>().position = resetVector; // player position set to myVector

        // Enable Inventory and Upgrades
        upgrade.SetActive(true);
        victory.SetActive(false);
        defeat.SetActive(false);

        // Enable all AI movement
        Time.timeScale = 0;

        // Enable player controller
        playerController.GetComponent<PlayerController>().enabled = true;

        // Reset bodyCount, enemiesSpawned and concurrentEnemies to zero
        bodyCount = 0;
        enemiesSpawned = 0;
        concurrentEnemies = 0;

        // Reset player main stats
        playerData.currentHealth = playerData.maxHealth;
        playerData.currentStamina = playerData.maxStamina;
        playerData.currentMana = playerData.maxMana;
    }
    // Coroutine to spawn enemies
    private IEnumerator SpawnEnemyEvent()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnDelay);
            // Check that enemyCount does not exceed maxEnemies
            if (maxEnemies > enemiesSpawned && concurrentEnemies < maxConcurrentEnemies)
            {
                spawnPoint = enemySpawns[Random.Range(0, enemySpawns.Count)]; // Random spawnPoint from list
                randomEnemy = enemyList[Random.Range(0, enemyList.Count)]; // Random enemy from list

                // Create enemyClone instance
                GameObject enemyClone = Instantiate(randomEnemy, spawnPoint.position, spawnPoint.rotation) as GameObject;

                enemiesSpawned++; // Increase enemyCount by one
                concurrentEnemies++; // Increase concurrent enemies by one
            }
        }
    }
    // Script to destroy all enemies, used to clear the arena after each round or player death
    void DestroyAllEnemies()
    {
        // Destroys enemy objects by looping through array
        enemyObjects = GameObject.FindGameObjectsWithTag("Enemy");

        for (var i = 0; i < enemyObjects.Length; i++)
        {
            Destroy(enemyObjects[i]);
        }
        // Destroys enemy attacks by looping through array
        enemyAttacks = GameObject.FindGameObjectsWithTag("EnemyAttack");

        for (var i = 0; i < enemyAttacks.Length; i++)
        {
            Destroy(enemyAttacks[i]);
        }
    }
    // Coroutine to increase wave difficulty and number
    public IEnumerator NextWave()
    {
        yield return new WaitForSeconds(1);
        // Increase max concurrent enemies
        maxConcurrentEnemies += concurrentIncrement;
        // Increase max enemies
        maxEnemies += maxEnemiesIncrement;
        // Increase wave number
        currentWave++;
    }
    // Creates a percentage of enemies remaining for HUD display
    public void UpdateEnemiesRemaining()
    {
        percentEnemiesRemaining = (maxEnemies - bodyCount) / maxEnemies;
    }
}
