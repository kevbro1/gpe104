﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Changes object to a random color. colorSpeed variable determines how fast this occurs.
public class ColorChange : MonoBehaviour
{
    // Declare variables
    private SpriteRenderer theRenderer; // Variable for the renderer
    public Color spriteColor; // Variable for the color
    public float colorSpeed; // Variable for the speed of the color change effect

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("ColorChangeEvent"); // Begin coroutine for color change event
    }
    // Color change event
    private IEnumerator ColorChangeEvent()
    {
        while (true)
        {
            yield return new WaitForSeconds(colorSpeed);
            spriteColor.r = Random.Range(0f, 1f); // Random red
            spriteColor.g = Random.Range(0f, 1f); // Random green
            spriteColor.b = Random.Range(0f, 1f); // Random blue
            spriteColor.a = 1.0f; // Alpha set to one
            // Load the SpriteRenderer component from the same object this component is on
            theRenderer = gameObject.GetComponent<SpriteRenderer>(); 
            if (theRenderer != null) // Check that theRenderer has been set
            {
                // Change the color value of the SpriteRenderer to new color
                theRenderer.color = spriteColor; 
            }
        }
    }
}
