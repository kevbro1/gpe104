﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Player block script
public class Block : MonoBehaviour
{
    private Collider2D blockCollider; // collider used for blocking
    private PlayerData playerData;
    private void Start()
    {
        blockCollider = GetComponent<Collider2D>(); // Get the collider for this object
        playerData = GameManager.instance.playerData;
    }
    // Player block method
    public void PlayerBlock()
    {
        if (blockCollider == null) // Check if the collider is null
        {
            Debug.Log("Null");
        }
       if (playerData.currentStamina > playerData.blockStaminaCost) // Check if stamina is sufficient
        {
            blockCollider.enabled = blockCollider.enabled; // Toggle the block collider
        }
    }
}
