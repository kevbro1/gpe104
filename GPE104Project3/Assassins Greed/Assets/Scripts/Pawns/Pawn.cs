﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Pawn super, includes basic movement
public abstract class Pawn : MonoBehaviour
{
    [HideInInspector] public Transform tf; // Transform component variable
    public float moveSpeed = 0.05f; // Movement Speed variable
    [HideInInspector] public GameObject sound; // Sound Radius GameObject
    [HideInInspector] public Transform p; // player transform component

    void Start()
    {
        tf = GetComponent<Transform>(); // Get the Transform Component
    }
    public virtual void MoveUp()
    {
        tf.Translate((Vector3.up * moveSpeed), Space.Self); // Relative movement up (in local space).
    }
    public virtual void MoveDown()
    {
        tf.Translate((Vector3.down * moveSpeed), Space.Self); // Relative movement down (in local space).
    }
    public virtual void MoveLeft()
    {
        tf.Translate((Vector3.left * moveSpeed), Space.Self); // Relative movement left (in local space).
    }
    public virtual void MoveRight()
    {
        tf.Translate((Vector3.right * moveSpeed), Space.Self); // Relative movement right (in local space).
    }
}
