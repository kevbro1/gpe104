﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Movement script for projectiles
public class ProjectileMovement : MonoBehaviour
{
    private Transform tf; // Transform component variable
    public float speed = 0.5f; // Movement Speed variable
    public float fireballDamage = 50; // Damage dealt by attack
    public PlayerData playerData; 
    void Start()
    {
        tf = GetComponent<Transform>(); // Transform component
        playerData = GameManager.instance.playerData; // Get Player data
    }

    void Update()
    {
        // Move towards target, used "right" because of how the sprite is oriented by default
        tf.Translate((Vector3.down * speed * Time.deltaTime), Space.Self);
    }
    // Projectile collider to deal damage to player
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player")) // Check if object is player
        {
            playerData.currentHealth -= fireballDamage; // Deal damage
            Destroy(this.gameObject); // Destroy object
        }
        else
        {
            Destroy(this.gameObject); // If the object is anything else, then destroy the projectile
        }
    }
}
