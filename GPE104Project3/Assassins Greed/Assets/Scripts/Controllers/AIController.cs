﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// AI Controller, includes AI Finite State Machine
public class AIController : Controller
{

    [HideInInspector] public string aiState = "patrolling"; // aiState for AI Finite State Machine

    // Start is called before the first frame update
    void Start()
    {
        guard = this.gameObject; // Assign guard to current guard.
        sight = guard.GetComponent<AISight>(); // AI sight component of the current guard
        hearing = guard.GetComponentInChildren<AIHearing>(); // AI hearing component of the current guard
        senses = guard.GetComponentInChildren<AISenseRange>(); // Range that sense scripts will operate
        guardPawn = guard.GetComponent<GuardPawn>(); // Guard Pawn component
    }

    // Update is called once per frame
    void Update()
    {
        // Patrolling state (player not heard or sighted)
        if (aiState == "patrolling") 
        {
            DoPatrol();
            Debug.Log("Patrolling");
            if (senses.inRange == true) // If player is inRange...
            {
                // If player is seen, transition to pursuing state
                if (sight.CanSee(GameManager.instance.playerObject) == true) 
                {
                    aiState = "pursuing";
                } 
            }
            // If the player is heard and the AI is in patrolling state then transition to alerted state
            if (hearing.canHear == true && aiState == "patrolling") 
            {
                aiState = "alerted";
            }
        }

        // Alerted state (player heard but not sighted)
        else if (aiState == "alerted")
        {
            DoAlert();
            Debug.Log("I heard something.");
            // When the player is out of sight and cannot be heard, then return to patrolling state
            if (sight.CanSee(GameManager.instance.playerObject) == false && hearing.canHear == false) 
            {
                Debug.Log("Must be nothing.");
                aiState = "patrolling";
            }
            // When the player is seen, transition to pursuing state
            if (sight.CanSee(GameManager.instance.playerObject) == true) 
            {
                aiState = "pursuing";
            }
        }

        // Pursuing state (player sighted)
        else if (aiState == "pursuing")
        {
            DoPursue();
            Debug.Log("There is the assassin, get him!");
            // When the player is out of sight and cannot be heard, the AI stops chasing and returns to patrol state
            if (sight.CanSee(GameManager.instance.playerObject) == false && hearing.canHear == false) 
            {
                aiState = "patrolling";
                Debug.Log("He is too fast... I need a break.");
            }
            // If the player cannot be seen, but can be heard then transition to alerted state
            if (sight.CanSee(GameManager.instance.playerObject) == false && hearing.canHear == true)
            {
                aiState = "alerted";
                Debug.Log("I think I heard him over here!");
            }
        }
    }

    public void DoPatrol() // Run if the AI does not see or hear player
    {
        guardPawn.Patrol(); // Patrol pathfinding function
    }
    public void DoAlert() // Run if AI hears but does not see player
    {
        guardPawn.RotateTowardsPlayer(); // AI rotates towards player
        guardPawn.GuardAlert(GameManager.instance.lastSoundLocation); // AI checks the sound
    }
    public void DoPursue() // Run if AI sees the player
    {
        guardPawn.RotateTowardsPlayer(); // AI rotates towards player
        guardPawn.GuardPursue(GameManager.instance.lastPlayerLocation); // AI chases player, goes to last known location
    }
    public void ChangeState(string newState)
    {
        aiState = newState; // Change the AI's state
    }
}
