﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enemy Pawn Script that is the parent to all enemy pawns
public class EnemyPawn : Pawn
{
    [HideInInspector] public AIController aiController; // AI Controller
    [HideInInspector] public PlayerPawn playerPawn; // Player pawn
    [HideInInspector] public Vector2 direction = new Vector2(1, 0); // Direction of the enemy pawn
    [HideInInspector] public Transform p; // Player transform component
    [HideInInspector] public TripleFire trip; // Triple attack component (May be used by multiple enemies)
    [HideInInspector] public Transform triptf; // Triple attack transform position (May be used by multiple enemies)
    public float moveSpeed = .01f; // Set the move speed

    public void Patrol()
    {
        // Method used to patrol between two points, not currently in use, but will be added in the future
    }
    public void Death()
    {
        // Explosion animation
        // Explosion sound
        // destroy object after 1 second
        Destroy(gameObject);
    }

    // Detect Collision with player or projectile
    private void OnCollisionEnter2D(Collision2D other) 
    {
        if (other.gameObject.CompareTag("Player")) // Collision with player
        {
            playerPawn.Death(); // Player pawn death method
        }
        if (other.gameObject.CompareTag("PlayerAttack")) // Collision with player
        {
            Death(); // Run Death method
        }
    }

    // Script used to flip the enemy
    public void FlipEnemy()
    {
        Vector3 scale = transform.localScale; // Set scale to localScale
        scale.x *= -1; // Multiply x by -1 to reverse
        transform.localScale = scale; // Set transform.localScale to new scale
    }
}
