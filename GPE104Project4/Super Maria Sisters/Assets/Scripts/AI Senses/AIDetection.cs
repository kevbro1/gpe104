﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is attached to AI pawns and is used to detect if the player is in sight
public class AIDetection : MonoBehaviour
{
    private Transform ttf; // Target transform component
    private Transform tf; // This objects transform component
    public float fieldOfView = 55.0f; // Set Field of View in Unity, default 55 degrees
    public float maxViewDistance = 0.5f; // Max view distance of the object, can be set in Unity
    public float angleToTarget; // Angle to the target used in raycast

    // Check if the player is visible by the AI
    public bool PlayerDetected(GameObject target)
    {
        ttf = target.GetComponent<Transform>(); // Get target transform component
        tf = GetComponent<Transform>(); // transform component

        // Find the vector from current object to target
        Vector2 vectorToTarget = ttf.position - tf.position;
        
        // Find the distance between the two vectors in float to compare with maxViewDistance
        float targetDistance = Vector2.Distance(ttf.position, tf.position);
        
        // Check if pawn is facing left or right then adjust the angleToTarget accordingly 
        if (transform.localScale.x > 0) 
        {
            angleToTarget = Vector2.Angle(vectorToTarget, tf.right); // if x > 0 angle should be to the right
        }
        else if (transform.localScale.x < 0)
        {
            angleToTarget = Vector2.Angle(vectorToTarget, -tf.right); // if x < 0 angle should be to the left
        }

            // If that angle is less than our field of view return true, else return false
            if (angleToTarget < fieldOfView && targetDistance < maxViewDistance)
        {
            int wallLayer = LayerMask.NameToLayer("Environment"); // Add Environment layer to variable
            int playerLayer = LayerMask.NameToLayer("Player"); // Add Player layer to variable
            int layerMask = (1 << playerLayer) | (1 << wallLayer); // Create layermask

            // RaycastHit from object to target, maxview distance, and only affects objects in layermask
            RaycastHit2D hit = Physics2D.Raycast(tf.position, vectorToTarget, maxViewDistance, layerMask);
            
            if (hit.collider.CompareTag("Player")) // If hit is player then...
            {
                return true;
            }
        }
        return false;
    }
}
