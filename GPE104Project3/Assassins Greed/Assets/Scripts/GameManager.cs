﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Game Manager singleton
public class GameManager : MonoBehaviour
{
    // Public static variables can be accessed anywhere, but can only be referenced. This means only one instance of the variable exists in memory
    public static GameManager instance; // Create a static variable of type "GameManager" (can create variable of the same type as a class)
    public GameObject playerObject; // The player game Object
    public List<GameObject> enemiesList; // List of enemies (asteroids and enemy ships)
    public List<Transform> enemySpawnList; // List of enemy spawn points
    public int maxEnemies = 4; // Sets maximum number of enemies allowed at once
    public int enemyCount = 0; // Keeps track of current number of enemies
    public Vector3 lastPlayerLocation; // Last player location vector from raycast
    public Vector3 lastSoundLocation; // Last sound location vector from raycast
    public List<Transform> patrolSpots; // Creates an array of spots that guards can move
    public string gameState = "active"; // Current game state. Default is active
    public GameObject pControllerObject; // Player Controller game object, playerController script is attached.
    public GameObject pauseMenu; // Pause menu game object
    public GameObject loseScreen; // Pause menu game object
    public GameObject winScreen; // Pause menu game object

    private void Awake()
    {
        // As long as there is not an instance already set, store the class component in the instance variable
        if (instance == null)
        {
            instance = this; // This is a reference to the current object, Store "this" instance of the class (component) in the instance variable. Singleton
            //DontDestroyOnLoad(gameObject); // Enable if you want the scene to remain constant after changing scenes.
        }
        else
        {
            Destroy(this.gameObject); // There can only be one instance, the second is destroyed and an error is displayed in console
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gameState = "active"; // Ensures game state set to active at start
        pControllerObject = GameObject.FindWithTag("GameController"); // Find PlayerController object
        pauseMenu = GameObject.FindWithTag("PauseMenu"); // Find PauseMenu object
        loseScreen = GameObject.FindWithTag("LoseScreen"); // Find PauseMenu object
        winScreen = GameObject.FindWithTag("WinScreen"); // Find PauseMenu object
        pauseMenu.SetActive(false); // Sets pauseMenu to inactive at start
        loseScreen.SetActive(false); // Sets loseScreen to inactive at start
        winScreen.SetActive(false); // Sets winScreen to inactive at start

    }

    // Update is called once per frame
    void Update()
    {
        if (gameState == "active") // Game in active state 
        {
            GameActive();
            if(Input.GetKey(KeyCode.Escape)) // Pause menu when escape is pressed
            {
                gameState = "pause";
            }
        }
        if (gameState == "win") // Game over state
        {
            GameWon();
            // Transitions in WinLose Script
        }
        if (gameState == "lose") // Game over state
        {
            GameLost();
            // Transitions in WinLose Script
        }
        if (gameState == "pause") // Game pause state
        {
            GamePaused();
            // Transitions in PauseMenu Script
        }
        if (gameState == "menu") // Game main menu state
        {
            MainMenu();
            gameState = "active"; // Sets gameState to active so it does not loop through win state
            // Transitions in MainMenu Script
        }
    }

    public void GameActive()
    {
        // Disable Pause Menu, Win Screen and Lose Screen
        pauseMenu.SetActive(false);

        //// Enable all AI movement
        Time.timeScale = 1;

        // Enable player controller
        pControllerObject.GetComponent<PlayerController>().enabled = true;
    }

    // Loads game win canvas
    public void GameWon()
    {
        // Enable Win Screen
        winScreen.SetActive(true);

        // Disable all AI movement
        Time.timeScale = 0;

        // Disable all player movement
        pControllerObject.GetComponent<PlayerController>().enabled = false;
    }

    // Loads game lose canvas
    public void GameLost()
    {
        // Enable Lose Screen
        loseScreen.SetActive(true);

        // Disable all AI movement
        Time.timeScale = 0;

        // Disable all player movement
        pControllerObject.GetComponent<PlayerController>().enabled = false;
    }

    // Loads game pause canvas and pauses game
    public void GamePaused()
    {
        // Enable Pause Menu
        pauseMenu.SetActive(true);

        // Disable all AI movement
        Time.timeScale = 0;

        // Disable all player movement
        pControllerObject.GetComponent<PlayerController>().enabled = false;
    }
    // Load Main Menu Scene
    public void MainMenu()
    {
        SceneManager.LoadScene(0); 
    }

    // Function to quit game
    public void GameQuit() // Game over state
    {
        Debug.Log("Quit Game"); // Destroy player
        Application.Quit(); // Quits game when out of lives
    }
}
