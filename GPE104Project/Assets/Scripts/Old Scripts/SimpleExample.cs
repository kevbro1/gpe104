﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleExample : MonoBehaviour
{
    public string theText = "Hello World";
    Vector2 myVector = new Vector2(2, -2); // Create a vector 2 units on x, and -2 units on y

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(theText); 
        Debug.Log(myVector.magnitude); // Output its magnitude
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    // OnCollisionEnter2D runs when a object (with a collider) collides with this object's collider
    void OnCollisionEnter2D(Collision2D otherObject)
    {
        Debug.Log("The GameObject of the other object is named: " + otherObject.gameObject.name);
    }
    // tShip = GameObject.FindGameObjectWithTag("Player").transform;
    // tf.rotation = Quaternion.RotateTowards(tf.rotation, targetShip.rotation, speed); // Will rotate how the ship rotates, rather than rotate towards the ship
    // tf.position = Vector3.MoveTowards(tf.position, targetShip.position, speed); // Move Towards target, but it will stop at last location if in start() or follow if in update()
}
