﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script to contain enemy movement within the collider trigger. The colliders are attached to the enemy spawns and can be adjusted to any size
public class MovementContainer : MonoBehaviour
{
    private AIDetection aiDetection;
    public void OnTriggerExit2D(Collider2D other)
    {
        aiDetection = other.gameObject.GetComponent<AIDetection>(); // AIDetection component
        // If other object is enemy and the player is not detected
        if (other.gameObject.CompareTag("Enemy") && aiDetection.PlayerDetected(GameManager.instance.playerObject) == false) 
        {
            // Calls flip enemy method, and reverses move speed and direction
            other.gameObject.GetComponent<EnemyPawn>().FlipEnemy();
            other.gameObject.GetComponent<EnemyPawn>().moveSpeed *= -1;
            other.gameObject.GetComponent<EnemyPawn>().direction *= -1;
        }
    }
}
