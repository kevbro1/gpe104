﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Used for player melee attacks
public class MeleeHit : MonoBehaviour
{
    public PlayerData playerData;
    // Melee attack collider
    private void OnTriggerEnter2D(Collider2D other)
    {
        playerData = GameManager.instance.playerData;
        if (other.gameObject.CompareTag("Enemy")) // If the other object is the enemy
        {
            GameManager.instance.soundManager.SoundBounce(); // Plays bounce sound when player hits an enemy
            other.gameObject.GetComponent<EnemyPawn>().currentHealth -= playerData.meleePower; // Deal damage
            other.gameObject.GetComponent<Rigidbody2D>().AddForce(-transform.up * playerData.meleeForce); // Add force to the attack
        }
    }
}
