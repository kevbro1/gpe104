﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Player Pawn
public class PlayerPawn : Pawn
{
    public PlayerController playerController;
    public PlayerData playerData;
    private Transform tf;
    private Animator ar;
    public Vector3 mousePosition; // Vector 3 of mouse position, used to rotate player
    public float mPosX; // X position of the mouse, used for animations
    public float mPosY; // Y position of the mouse, used for animations
    public string animState = "standing"; // Used for animation state machine

    void Start()
    {
        ar = GetComponentInChildren<Animator>();
        tf = GetComponent<Transform>();
    }

    void Update()
    {
        mousePosition = Input.mousePosition; // Create a vector3 with the mouse position in screenspace
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition); // Converts to worldspace
        // Store the last Y and X mouse axis variables. This will determine the direction of the animation blend
        if (Input.GetAxisRaw("Mouse Y") != 0)
        {
            mPosY = Input.GetAxisRaw("Mouse Y");
        }
        if (Input.GetAxisRaw("Mouse X") != 0)
        {
            mPosX = Input.GetAxisRaw("Mouse X");
        }
        ar.SetFloat("xInput", mPosX); // Set the animation float to mouse position X
        ar.SetFloat("yInput", mPosY); // Set the animation float to mouse position Y

        // Animation States for the player
        if (animState == "standing")
        {
            AnimStanding();
        }
        if (animState == "walking")
        {
            AnimWalking();
        }
        if (animState == "running")
        {
            AnimRunning();
        }
        if (animState == "melee")
        {
            AnimMelee();
        }
    }
    // Walk forward
    public void Forward()
    {
        animState = "walking";
        tf.Translate((Vector3.up * playerData.walkSpeed * Time.deltaTime), Space.World);
    }
    // Walk backward
    public void Back()
    {
        animState = "walking";
        tf.Translate((Vector3.down * playerData.walkSpeed * Time.deltaTime), Space.World);
    }
    // Strafe left
    public void Left()
    {
        animState = "walking";
        tf.Translate((Vector3.left * playerData.walkSpeed * Time.deltaTime), Space.World);
    }
    // Strafe right
    public void Right()
    {
        animState = "walking";
        tf.Translate((Vector3.right * playerData.walkSpeed * Time.deltaTime), Space.World);
    }
    // Run forward
    public void RunForward()
    {
        if(playerData.currentStamina > 0) // Check player stamina
        {
            animState = "running";
            tf.Translate((Vector3.up * playerData.runSpeed * Time.deltaTime), Space.World);
            playerData.currentStamina -= playerData.staminaReductAmount;
        }
        if (playerData.currentStamina == 0) // If stamina = zero player will walk instead
        {
            animState = "walking";
            tf.Translate((Vector3.up * playerData.walkSpeed * Time.deltaTime), Space.World);
        }
    }
    // Backpeddle
    public void RunBack()
    {
        if (playerData.currentStamina > 0) // Check player stamina
        {
            animState = "running";
            tf.Translate((Vector3.down * playerData.runSpeed * Time.deltaTime), Space.World);
            playerData.currentStamina -= playerData.staminaReductAmount;
        }
        if (playerData.currentStamina == 0) // If stamina = zero player will walk instead
        {
            animState = "walking";
            tf.Translate((Vector3.down * playerData.walkSpeed * Time.deltaTime), Space.World);
        }
    }
    // Quick strafe left
    public void RunLeft()
    {
        if (playerData.currentStamina > 0) // Check player stamina
        {
            animState = "running";
            tf.Translate((Vector3.left * playerData.runSpeed * Time.deltaTime), Space.World);
            playerData.currentStamina -= playerData.staminaReductAmount;
        }
        if (playerData.currentStamina == 0) // If stamina = zero player will walk instead
        {
            animState = "walking";
            tf.Translate((Vector3.left * playerData.walkSpeed * Time.deltaTime), Space.World);
        }
    }
    // Quick strafe right
    public void RunRight()
    {
        if (playerData.currentStamina > 0) // Check player stamina
        {
            animState = "running";
            tf.Translate((Vector3.right * playerData.runSpeed * Time.deltaTime), Space.World);
            playerData.currentStamina -= playerData.staminaReductAmount;
        }
        if (playerData.currentStamina == 0) // If stamina = zero player will walk instead
        {
            animState = "walking";
            tf.Translate((Vector3.right * playerData.walkSpeed * Time.deltaTime), Space.World);
        }
    }
    // Play standing animation
    public void Standing()
    {
        animState = "standing";
    }
    // Run melee attack
    public void MeleeAttack()
    {
        animState = "melee"; // Melee animation. Need to get the full animation to play
        GetComponentInChildren<MeleeSource>().MeleeAttack(); // Run attack script in child component
    }
    // Block enemy attacks
    public void Block()
    {
        GetComponentInChildren<Block>().PlayerBlock(); // Run block script in child component
    }
    // Skill one (Tornado attack)
    public void Skill1()
    {
        GetComponentInChildren<TornadoSource>().LaunchTornado(); // Run tornado script in child component
    }
    // Unused Skill
    public void Skill2()
    {
        Debug.Log("Skill 2");
    }
    // Unused Skill
    public void Skill3()
    {
        Debug.Log("Skill 3");
    }
    // Unused Skill
    public void Skill4()
    {
        Debug.Log("Skill 4");
    }
    // Face the player sprite in the direction of the mouse
    public void FaceMouseDir()
    {
        Vector2 direction = new Vector2(mousePosition.x - tf.position.x, mousePosition.y - tf.position.y); // Set the direction to the mouse position on the x and y axis
        tf.up = -direction; // Sets the transform direction to direction vector. Negative direction because default sprite faces down.
        // ***Ideally use slerp to smooth player rotation***
    }

    // Player methods run during animation states
    public void AnimStanding()
    {
        ar.Play("Standing_S");
    }
    public void AnimWalking()
    {
        ar.Play("Walking");
    }
    public void AnimMelee()
    {
        ar.Play("MeleeAttack");
    }
    public void AnimRunning()
    {
        ar.Play("Running");
    }
}
