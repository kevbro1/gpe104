﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Guard pawn child of EnemyPawn, includes pursue and alert functions
public class GuardPawn : EnemyPawn
{
    
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Get the Transform Component
        p = GameManager.instance.playerObject.GetComponent<Transform>(); // Player transform component
    }
    public void GuardPursue(Vector3 playerLocation) // Guard Pursue function. Chase player
    {
        tf.position = Vector2.MoveTowards(tf.position, playerLocation, moveSpeed * Time.deltaTime); // Guard chases player, will go to last known location
    }
    public void GuardAlert(Vector3 soundLocation) // Guard alerted function. Move towards sound
    {
        tf.position = Vector2.MoveTowards(tf.position, soundLocation, walkSpeed * Time.deltaTime); // Guard chases player, will go to last known location
    }
}
