﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Basic Movement Controls
public class SpriteMovementOld : MonoBehaviour
{
    private Transform tf; // Transform component variable
    public float speed; // Movement Speed variable

    // Start is called before the first frame update
    void Start()
    {
        speed = 0.1f; // Set Speed variable
        tf = GetComponent<Transform>(); // Get the Transform Component
    }

    // Update is called once per frame
    void Update()
    {
        // Move up when Up Arrow key is pressed without left shift key
        if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + (Vector3.up * speed); // Vector3.up multiplied by speed variable
        }
        // Move down when Down Arrow key is pressed without left shift key
        if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + (Vector3.down * speed); // Vector3.down multiplied by speed variable
        }
        // Move left when Left Arrow key is pressed without left shift key
        if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + (Vector3.left * speed); // Vector3.left multiplied by speed variable
        }
        // Move right when Right Arrow key is pressed without left shift key
        if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + (Vector3.right * speed); // Vector3.right multiplied by speed variable
        }
        // Shift + UpArrow Key moves one unit then stops
        if (Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + Vector3.up; // Vector3.up is a preset Vector of (0,1,0)
        }
        // Shift + DownArrow Key moves one unit then stops
        if (Input.GetKeyDown(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + Vector3.down; // Vector3.down is a preset Vector of (0,-1,0) 
        }
        // Shift + LeftArrow Key moves one unit then stops
        if (Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + Vector3.left; // Vector3.left is a preset Vector of (-1,0,0) 
        }
        // Shift + RightArrow Key moves one unit then stops
        if (Input.GetKeyDown(KeyCode.RightArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + Vector3.right; // Vector3.right is a preset Vector of (1,0,0) 
        }
    }
}
