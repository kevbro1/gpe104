﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Basic Movement Controls
public class SpriteMovement : MonoBehaviour
{
    private Transform tf; // Transform component variable
    public float moveSpeed = 0.3f; // Movement Speed variable
    public float rotationSpeed = 1.5f; // Create a variable for rotation in degrees, per frame draw

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Get the Transform Component
    }

    // Update is called once per frame
    void Update()
    {
        // Move up when Up Arrow key is pressed without left shift key
        if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.LeftShift))
        {
            tf.Translate((Vector3.right * moveSpeed), Space.Self); // Relative movement up (in local space). Right is "Up" because the sprite faces the right by default
        }
        // Move down when Down Arrow key is pressed without left shift key
        if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.LeftShift))
        {
            tf.Translate((Vector3.left * moveSpeed), Space.Self); // Relative movement up (in local space). Left is "Down" because the sprite faces the right by default
        }
        // Rotate counter clockwise when Left Arrow key is pressed without left shift key
        if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.LeftShift))
        {
            tf.Rotate(0, 0, rotationSpeed); // Rotate along Z axis
        }
        // Rotate clockwise when Right Arrow key is pressed without left shift key
        if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftShift))
        {
            tf.Rotate(0, 0, -rotationSpeed); // Rotate along Z axis (negative)
        }
        // Shift + UpArrow Key moves one unit then stops
        if (Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + Vector3.up; // Vector3.up is a preset Vector of (0,1,0)
        }
        // Shift + DownArrow Key moves one unit then stops
        if (Input.GetKeyDown(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + Vector3.down; // Vector3.down is a preset Vector of (0,-1,0) 
        }
        // Shift + LeftArrow Key moves one unit then stops
        if (Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + Vector3.left; // Vector3.left is a preset Vector of (-1,0,0) 
        }
        // Shift + RightArrow Key moves one unit then stops
        if (Input.GetKeyDown(KeyCode.RightArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            tf.position = tf.position + Vector3.right; // Vector3.right is a preset Vector of (1,0,0) 
        }
    }
}
