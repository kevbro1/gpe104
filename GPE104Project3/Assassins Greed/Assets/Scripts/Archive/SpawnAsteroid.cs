﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAsteroid : MonoBehaviour
{
    private Transform tf; // Transform component of enemyShip
    public GameObject asteroid;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Get the Transform Component of enemy ship
        // Create asteroidClone instance
        GameObject asteroidClone = Instantiate(asteroid, tf.position, tf.rotation) as GameObject;
    }
    // Update is called once per frame
    void Update()
    {

    }
}
