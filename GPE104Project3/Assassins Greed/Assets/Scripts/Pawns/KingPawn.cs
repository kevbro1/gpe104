﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enemy King child of EnemyPawn, includes player collide(win) function
public class KingPawn : EnemyPawn
{
    // Detect if player collides with King "Killing the King"
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameManager.instance.gameState = "win"; // If the player collides with the king change the game state to win
        }
    }
}
