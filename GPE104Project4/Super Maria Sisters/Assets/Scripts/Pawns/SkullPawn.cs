﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Orc Pawn script
public class SkullPawn : EnemyPawn
{
    public float rotationSpeed = 1; // Set rotation speed of the skull attack
    public float attackSpeed = 1.1f; // Set the heat seeking attack speed
    private Vector3 attackDirection; // Set the attack direction

    void Start()
    {
        tf = GetComponent<Transform>(); // Transform component
        rb2d = GetComponent<Rigidbody2D>(); // Rigidbody component
        sm = GameManager.instance.soundManager; // Sound Manager component attached to main camera
        p = GameManager.instance.playerObject.GetComponent<Transform>(); // Player transform component
        playerPawn = GameManager.instance.playerObject.GetComponent<PlayerPawn>(); // Player pawn component

        // COMPONENTS THAT MIGHT BE USED IN THE FUTURE
        //sr = GetComponent<SpriteRenderer>(); // Sprite Renderer component
        //ar = GetComponent<Animator>(); // Animator component
        //gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>(); // Game Manager component on Game Manager object
        //aiController = GetComponent<SkullController>(); // AI controller component
        
    }

    // Skull heat seeking attack method
    public void HeatSeekAttack()
    {
        attackDirection = (tf.position - p.position); // Find the player direction
        tf.position -= attackDirection * attackSpeed * Time.deltaTime; // Move the skull in the player's direction
        sm.SoundSkull(); // Play the skull sound
    }
    void FixedUpdate()
    {
        rb2d.velocity = new Vector2(moveSpeed, rb2d.velocity.y); // Skull patrol movement
    }
}
