﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Run the player death script if the player collides with the collider attached to this object
public class CollisionKillPlayer : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D other)
    {
       if (other.gameObject == GameManager.instance.playerObject) // Make sure the object is the player
        {
            GameManager.instance.playerObject.GetComponent<PlayerPawn>().Death(); // Kill the player
        }
    }
}
