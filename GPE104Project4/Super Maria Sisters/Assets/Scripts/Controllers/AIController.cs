﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// AI Controller is the parent to all other AI controllers
public class AIController : Controller
{
    [HideInInspector] public string aiState = "patrolling";
    [HideInInspector] public AIDetection aiDetection;

    // AI state functions
    public virtual void DoPatrol()
    {
        // Patrol between two points
    }
    public virtual void DoAttack()
    {
        // Identify player and attack
    }
    public virtual void DoDead()
    {
        // Death animaiton, sound and destroy object
    }
}
