﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteTest : MonoBehaviour
{
    //Declare variables
    private SpriteRenderer theRenderer; //variable for our renderer
    public Color spriteColor; //variable for our color
    private Transform tf; // A variable to hold our Transform component
    private float speed;
    // Start is called before the first frame update
    public float maxScale; // Create a variable for the max we can scale in one frame draw
    void Start()
    {
        //Load the SpriteRenderer component from the same object this component is on
        theRenderer = gameObject.GetComponent<SpriteRenderer>();
        //Change the color from our color picker so that the alpha is 1
        spriteColor.a = 1.0f;
        //As long as theRenderer has been set
        if (theRenderer != null)
        {
            //Change the color value of the SpriteRenderer to new color
            theRenderer.color = spriteColor;
        }
        speed = 0.01f;
        // Get the Transform Component
        tf = GetComponent<Transform>();
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 myVector = new Vector3(2, 4, 12);
        myVector = myVector.normalized; // You could also call the function myVector.Normalize();
        tf.position = tf.position + (myVector * speed);
        // Move up every frame draw by adding 1 to the y of our position
        //tf.position = tf.position + (Vector3.up * 0.5f); // Vector3.up is a preset Vector of (0,1,0) 
        // There is also a Vector3.right (1,0,0) and Vector3.forward (0,0,1)  


        // Find out how far and in which direction our axes are pressed
        float axesValue = Input.GetAxis("scale"); // This is between -1 and 1 -- a "percent"
        // Use that value to calculate a value between -100% and 100% of our max scale per frame
        float scaleAmount = axesValue * maxScale;
        // Increase the scale of our object by that amount on all axes (1,1,1) * scaleAmount
        tf.localScale += Vector3.one * scaleAmount;
    }
}
