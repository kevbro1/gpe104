﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// AI controller parent
public abstract class AIController : MonoBehaviour
{
    public string aiState = "idle";
    public virtual void DoIdle()
    {
        // AI Doing nothing, may need for animation
    }
}
