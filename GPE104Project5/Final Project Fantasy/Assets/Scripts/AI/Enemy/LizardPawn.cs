﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lizard Pawn
public class LizardPawn : EnemyPawn
{
    // Run Lizard attack
    public void AttackPlayer()
    {
        p = player.GetComponent<Transform>(); // Grab player transform
        tf.position = Vector3.MoveTowards(tf.position, p.position, attackSpeed * Time.deltaTime); // Rotate towards the player
        GetComponentInChildren<MeleeSource>().MeleeAttack(); // Run the melee attack on child object
        // Plays yell sound when lizard attacks
        GameManager.instance.soundManager.SoundYell();
    }
}
