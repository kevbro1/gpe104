﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Used to collect player inputs
public class PlayerController : Controller
{
    public float inputHorizontal; // Variable for horizontal inputs
    public float inputVertical; // Variable for vertical inputs
    [HideInInspector] public PlayerPawn playerPawn; // Player pawn
  
    void Start()
    {
        playerPawn = GameManager.instance.playerObject.GetComponent<PlayerPawn>(); // Sets player pawn
    }


    void FixedUpdate()
    {
        
        // Input for Walk
        if (Input.GetButton("Horizontal") && !Input.GetButton("Run"))
        {
            playerPawn.FlipPawn(); // Flip pawn method called
            inputHorizontal = Input.GetAxis("Horizontal");
            playerPawn.Walk(); // Walk method called
        }

        // Input for Run
        if (Input.GetButton("Horizontal") && Input.GetButton("Run"))
        {
            playerPawn.FlipPawn(); // Flip pawn method called
            inputHorizontal = Input.GetAxis("Horizontal");
            playerPawn.Run(); // Run method called
        }

        // No horizontal input = stand
        if (!Input.GetButton("Horizontal"))
        {
            playerPawn.Stand();
        }

        // Input for Jump
        if (Input.GetButtonDown("Jump") && playerPawn.jumpCount < playerPawn.maxJumps)
        {
            playerPawn.Jump(); 
            playerPawn.jumpCount++; // Counts jumps, default max jumps = 2
        }

        // Input for shoot
        if (Input.GetButtonDown("Fire1"))
        {
            playerPawn.ProjectileAttack(); // Launch Fireball
        }
    }
}
