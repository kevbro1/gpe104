﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Game Manager singleton
public class GameManager : MonoBehaviour
{
    // Public static variables can be accessed anywhere, but can only be referenced. This means only one instance of the variable exists in memory
    public static GameManager instance; // Create a static variable of type "GameManager" (can create variable of the same type as a class)
    public int maxEnemies = 3; // Sets maximum number of enemies allowed at once
    public int enemyCount = 0; // Keeps track of current number of enemies
    public int lives = 3; // Number of lives
    public int maxPowerUps = 1; // Maximum number of power ups
    public int activePowerUps = 0; // Number of active power ups
    public List<GameObject> enemiesList; // List of enemies (asteroids and enemy ships)
    public List<GameObject> powerUpList; // List of power ups
    public List<Transform> enemySpawnList; // List of enemy spawn points
    public List<Transform> powerUpSpawnList; // List of power up spawn points
    private GameObject player; // The player game Object


    private void Awake()
    {
        // As long as there is not an instance already set, store the class component in the instance variable
        if (instance == null)
        {
            instance = this; // This is a reference to the current object, Store "this" instance of the class (component) in the instance variable. Singleton
            DontDestroyOnLoad(gameObject); // Don't delete this object if we load a new scene
        }
        else
        {
            Destroy(this.gameObject); // There can only be one instance, the second is destroyed and an error is displayed in console
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player"); // Set player to GameObject with "player" tag (Must tag the player in Unity)
    }
    // Update is called once per frame
    void Update()
    {
        if (lives <= 0) // If out of lives, destroy the player (so it shows while in editor) and quit game.
        {
            GameOver();
        }
    }
    // Function to end game
    public void GameOver()
    {
        Destroy(player.gameObject); // Destroy player
        Application.Quit(); // Quits game when out of lives
    }
}
