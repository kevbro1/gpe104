﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Movement of projectiles fired by player
public class ProjectileMovement : MonoBehaviour
{
    private Transform tf; // Transform component variable
    public float speed = 0.2f; // Movement Speed variable

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Transform component
    }

    // Update is called once per frame
    void Update()
    {
        // Move towards target, used "right" because of how the sprite is oriented by default
        tf.Translate((Vector3.right * speed), Space.Self);
    }
}
