﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Main Menu button functions
public class MainMenu : MonoBehaviour
{
    // Start new game
    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    // Quit game
    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}

