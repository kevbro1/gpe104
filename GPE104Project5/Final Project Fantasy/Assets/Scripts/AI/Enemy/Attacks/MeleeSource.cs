﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Source of melee attack
public class MeleeSource : MonoBehaviour
{
    public GameObject melee; // Set the projectile in the inspector
    public float meleeDuration = 1; // Duration that projectile exists
    private Transform tf;
    private void Start()
    {
        tf = GetComponent<Transform>(); // Get transform component
    }
    public void MeleeAttack()
    {
        // Create a melee instance
        GameObject meleeClone = Instantiate(melee, tf.position, tf.rotation) as GameObject;
        if (meleeClone != null) // Check if projectileClone exists
        {
            Destroy(meleeClone, meleeDuration); // Destroy projectile after projectileDuration
        }
    }
}
