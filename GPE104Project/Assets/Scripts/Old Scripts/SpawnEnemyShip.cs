﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemyShip : MonoBehaviour
{
    public GameObject enemyShip; // Set enemy ship GameObject in Unity
    private Transform tf; // Transform component of enemyShip
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Get the Transform Component of enemy ship
        // Create enemyClone instance
        GameObject enemyClone = Instantiate(enemyShip, tf.position, tf.rotation) as GameObject;
    }
    // Update is called once per frame
    void Update()
    {

    }
}
