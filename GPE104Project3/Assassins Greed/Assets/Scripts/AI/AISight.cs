﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// AI sight script, includes angle, distance, and raycast from AI to player
public class AISight : MonoBehaviour
{
    private Transform ttf; // Target transform component
    private Transform tf; // This objects transform component
    public float fieldOfView = 55.0f; // Set Field of View in Unity, default 45 degrees
    public float maxViewDistance = 5.0f; // Max view distance of the object, can be set in Unity
    [HideInInspector] public Vector3 lastPlayerLocation; // lastPlayerLocation vector set by raycast.point

    public bool CanSee(GameObject target)
    {
        ttf = target.GetComponent<Transform>(); // Get target transform component
        tf = GetComponent<Transform>(); // transform component

        // Find the vector from current object to target
        Vector2 vectorToTarget = ttf.position - tf.position;

        // Find the distance between the two vectors in float to compare with maxViewDistance
        float targetDistance = Vector2.Distance(ttf.position, tf.position);

        // Find the angle between the direction our agent is facing (forward in local space) and the vector to the target.
        float angleToTarget = Vector2.Angle(vectorToTarget, -tf.up);

        // If that angle is less than our field of view return true, else return false
        if (angleToTarget < fieldOfView && targetDistance < maxViewDistance)
        {
            int wallLayer = LayerMask.NameToLayer("Walls"); // Add Walls layer to variable
            int playerLayer = LayerMask.NameToLayer("Player"); // Add Player layer to variable
            int layerMask = (1 << playerLayer) | (1 << wallLayer); // Create layermask

            // RaycastHit from object to target, maxview distance, and only affects objects in layermask
            RaycastHit2D hit = Physics2D.Raycast(tf.position, vectorToTarget, maxViewDistance, layerMask);

            lastPlayerLocation = hit.point; // Set lastPlayerLocation to raycast hit point
            GameManager.instance.lastPlayerLocation = lastPlayerLocation; // Set lastPlayerLocation in GameManager

            Debug.DrawRay(tf.position, vectorToTarget, Color.red, maxViewDistance); // Draw rays

            if (hit.collider.CompareTag("Player")) // If hit is player then...
                {
                    return true;
                }
        }
        return false;
    }
}
