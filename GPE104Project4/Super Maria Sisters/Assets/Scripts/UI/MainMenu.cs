﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Main menu script
public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene(1); // Load level 1
    }
    public void QuitGame()
    {
        Application.Quit(); // Quit game
    }
}
