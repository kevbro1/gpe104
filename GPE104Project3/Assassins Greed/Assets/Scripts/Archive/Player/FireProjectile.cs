﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script for player firing projectiles
public class FireProjectile : MonoBehaviour
{
    private Transform tf; // Transform component of projectile
    public float projectileDuration; // Duration that projectile exists
    public GameObject projectile; // Public GameObject should be set to desired projectile

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Get the Transform Component of enemy ship
    }
    // Update is called once per frame
    void Update()
    {
        // Fire projectile when Spacebar is pressed
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (projectile != null) // Check if projectile object is set
            {
                // Creates a projectile instance with proper orientation
                GameObject projectileClone = Instantiate(projectile, tf.position, tf.rotation) as GameObject;
                if (projectileClone != null) // Check if projectileClone exists
                {
                    Destroy(projectileClone, projectileDuration); // Destroy projectile after projectileDuration
                }
            }
        }     
    }
}
