﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Menu options on the victory and defeat screens
public class VictoryDefeat : MonoBehaviour
{
    public void MainMenu()
    {
        SceneManager.LoadScene(0); // Load the main menu
    }
    public void QuitGame()
    {
        Application.Quit(); // Quit the game
    }
}
