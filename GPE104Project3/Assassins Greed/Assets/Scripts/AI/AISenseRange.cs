﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Sets the range for AI sensory functions to activate and returns true if in range, or false if out of range
public class AISenseRange : MonoBehaviour
{
    private GameObject senses; // Game object for sense object that hold sense collider
    [HideInInspector] public bool inRange; // Is the player inRange of sensory information

    private void Start()
    {
        senses = GameObject.FindWithTag("Senses"); // Set senses to GameObject with "senses" tag
    }
    public void OnTriggerEnter2D(Collider2D other) // OnTriggerEnter for intersecting sense collider
    {

        if (other.gameObject == senses) // If game object is tagged with senses
        {
            inRange = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)  // OnTriggerExit for intersecting sense collider
    {
        if (other.gameObject == senses) // If game object is tagged with senses
        {
            inRange = false;
        }
    }
}
