﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enemy Pawn child of Pawn, includes rotation, patrol and collides with player(lose) functions
public abstract class EnemyPawn : Pawn
{
    public float rotationSpeed = 1.0f; // Rotation speed
    public float walkSpeed; // Sets walk speed for alert function
    public float patrolSpeed; // Sets patrol speed for enemies
    private int randomSpot; // Picks a random patrolSpot for the guard to walk to
    private float waitTime; // Time the guard will wait at a patrolSpot
    public float startWaitTime = 10; // Default amount of time that a guard will wait at a patrolSpot
    public float patrolSpotThreshold = 0.5f; // Distance from the patrolSpot the enemy pawn needs to be for it to "reach" that patrolSpot

    void Start()
    {
        tf = GetComponent<Transform>(); // Get the Transform Component
        p = GameManager.instance.playerObject.GetComponent<Transform>(); // Player transform component
        waitTime = startWaitTime; // Set waitTime to default startWaitTime
    }
    private void Awake()
    {
        randomSpot = Random.Range(0, GameManager.instance.patrolSpots.Count); // Sets randomSpot to a random patrolSpot
    }

    public void RotateTowardsPlayer() // Rotate towards player
    {
        Vector2 targetVector = p.position - tf.position; // Calculate Vector between AI and player
        float angle = Mathf.Atan2(targetVector.x, targetVector.y) * Mathf.Rad2Deg; // Calculate the angle between x and y axis of targetVector
        // Set rotation based on angle and Vector 3 axis ("back" based on enemyShip orientation)
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        // Slerp between current rotation angle and desired rotation "shipRotation" and apply rotationSpeed
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationSpeed);
    }
    public void Patrol() // Patrol script to move between patrol spots
    {
        // Move the AI towards a random "patrolSpot" at the patrolSpeed
        transform.position = Vector2.MoveTowards(transform.position, GameManager.instance.patrolSpots[randomSpot].position, patrolSpeed * Time.deltaTime);

        // If the distance between the AI and patrolSpot is less than the patrol spot threshold
        if (Vector2.Distance(transform.position, GameManager.instance.patrolSpots[randomSpot].position) < patrolSpotThreshold)
        {
            if (waitTime <= 0) // Waits for the waitTime amount to be zero before moving again
            {
                randomSpot = Random.Range(0, GameManager.instance.patrolSpots.Count);
                waitTime = startWaitTime;
            }
            else // Decrement waitTime
            {
                waitTime -= Time.deltaTime;
            }
        }
    }
    void OnCollisionEnter2D(Collision2D other) // If the enemy pawn collides with the player, the player loses
    {
        if (other.gameObject.tag == "Player")
        {
            GameManager.instance.gameState = "lose"; // Change the gameState to lose
        }
    }
}
