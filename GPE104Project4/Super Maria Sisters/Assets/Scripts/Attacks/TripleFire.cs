﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is the triple fire attack used by the robot. It shoots three projectiles in different directions
public class TripleFire : MonoBehaviour
{
    // Set each projectile individually. Allows for multiple variations
    public GameObject projectile1;
    public GameObject projectile2;
    public GameObject projectile3;

    public float projectileDuration; // Duration that projectile exists
    private Transform tf; // Transform component
    public Quaternion tfHigh; // Quaternion for the high angle shot
    public Quaternion tfLow; // Quaternion for the low angle shot
    public float attackDelay = 10; // Delay time between attacks
    public float lastAttack = 0; // Used in loop for attack delay
    private SoundManager sm; // Sound manager component

    void Start()
    {
        tf = GetComponent<Transform>(); // Transform component
        sm = GameManager.instance.soundManager; // Sound Manager component attached to main camera
    }

    public void LaunchProjectile()
    {
        if (Time.time > lastAttack + attackDelay) // Delays the attack using Time.time as a reference
        {
            sm.SoundLaser(); // Laser sound is called

            // Creates three projectile instances with proper orientation
            GameObject projectileClone2 = Instantiate(projectile2, tf.position, tfHigh) as GameObject;
            GameObject projectileClone1 = Instantiate(projectile1, tf.position, tf.rotation) as GameObject;
            GameObject projectileClone3 = Instantiate(projectile3, tf.position, tfLow) as GameObject;

            // Destroy all projectiles after projectileDuration
            Destroy(projectileClone1, projectileDuration);
            Destroy(projectileClone2, projectileDuration);
            Destroy(projectileClone3, projectileDuration);

            lastAttack = Time.time; // Reset lastAttack after attack.
        }
    }
}
