﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Orc controller, includes Orc finite states
public class OrcController : AIController
{
    [HideInInspector] public OrcPawn orcPawn;

    void Start()
    {
        aiDetection = GetComponent<AIDetection>(); // AIDetection component
        orcPawn = GetComponent<OrcPawn>(); //Orc Pawn component
    }

    void Update()
    {
        // Orc Finite State Machine
        if (aiState == "patrolling")
        {
            DoPatrol();
            // Transition to Attacking
            if (aiDetection.PlayerDetected(GameManager.instance.playerObject) == true) // Check that player is detected
            {
                aiState = "attacking";
            }
        }
        if (aiState == "attacking")
        {
            DoAttack();
            // Transition to patrolling
            if (aiDetection.PlayerDetected(GameManager.instance.playerObject) == false) // Check that player is not detected
            {
                aiState = "patrolling";
            }
        }
        if (aiState == "dead")
        {
            // Run if orc dies
            DoDead();
        }
    }
    public override void DoAttack()
    {
        orcPawn.ChargeAttack(); // Calls orc's charge attack
    }
}
