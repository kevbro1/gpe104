﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Camera Follow Script 
public class CameraFollow : MonoBehaviour
{
    private Transform tf; // Transform component variable
    public GameObject followObject; // GameObject variable
    public float speed = 0.5f; // Speed multiplier used to calculate interpolation value

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Get the Transform Component
    }

    // Update is called once per frame
    void Update()
    {
        if (followObject != null)
        {
            float interpol = speed * Time.deltaTime; // Calculate interpolation value
            Vector3 camera = this.transform.position; // Camera position
            // Lerp allows camera to follow, but lag and catch up to object. Equation = CameraPosition + ((FollowObjectPosition - CameraPosition) * Interpol)
            camera.y = Mathf.Lerp(this.transform.position.y, followObject.transform.position.y, interpol);
            camera.x = Mathf.Lerp(this.transform.position.x, followObject.transform.position.x, interpol);
            this.transform.position = camera; // Camera position set to Lerp camera values
        }
    }
}
