﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Pause Menu button functions
public class PauseMenu : MonoBehaviour
{
    // Change game state to menu
    public void MainMenu()
    {
        GameManager.instance.gameState = "menu"; 
    }

    // Change game state to active
    public void Resume()
    {
        GameManager.instance.gameState = "active";
    }

    // Quit game
    public void Quit()
    {
        GameManager.instance.GameQuit(); 
    }
}
