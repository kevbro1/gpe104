﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Player HUD script that displays lives and fireballs remaining
public class HUD : MonoBehaviour
{
    public Text lifeCount; // Life Count Text string. Set text object to update in Unity
    public Text fireballCount; // Fireball Count Text string.

    void Update()
    {
        // Print lives to screen. Typecast int to string
        lifeCount.text = GameManager.instance.playerLives.ToString(); // Set lifecount
        fireballCount.text = GameManager.instance.fireballCount.ToString(); // Set fireball count
    }
}
