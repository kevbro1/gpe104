﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIVision : MonoBehaviour
{
    private GameObject player;
    private void Start()
    {
        player = GameObject.FindWithTag("Player"); // Set player to GameObject with "player" tag (Must tag the player in Unity)
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
            Debug.Log("Player in view");
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
            Debug.Log("Player out of view");
    }
}
