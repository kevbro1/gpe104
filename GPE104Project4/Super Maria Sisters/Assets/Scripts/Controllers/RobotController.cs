﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Includes Robot Finite States
public class RobotController : AIController
{
    [HideInInspector] public RobotPawn robotPawn;
    void Start()
    {
        aiDetection = GetComponent<AIDetection>(); // AI Detection component
        robotPawn = GetComponent<RobotPawn>(); // Robot Pawn component
    }

    // Update is called once per frame
    void Update()
    {
        // AI Finite State Machine
        if (aiState == "patrolling")
        {
            DoPatrol();
            // Transition to Attacking
            if (aiDetection.PlayerDetected(GameManager.instance.playerObject) == true) // Check that player is detected
            {
                aiState = "attacking";
            }
        }
        if (aiState == "attacking")
        {
            DoAttack();
            aiState = "patrolling";
            // Transition to patrolling
            if (aiDetection.PlayerDetected(GameManager.instance.playerObject) == false) // Check that player is not detected
            {
                aiState = "patrolling";
            }
        }
        if (aiState == "dead")
        {
            // Call if robot is dead
            DoDead();
        }
    }
    public override void DoAttack()
    {
        robotPawn.TripleFireAttack(); // Calls the triple fire attack method
    }
}
