﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script launches a projectile in the forward direction for a set duration
public class ProjectileLaunch : MonoBehaviour
{
    public GameObject projectile; // Set the projectile in the inspector
    public float projectileDuration; // Duration that projectile exists
    private Transform tf;
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Get transform component
    }

    // Launch projectile method
    public void LaunchProjectile()
    {
        if (projectile != null) // Check if projectile object is set
        {
            // Creates a projectile instance with proper orientation
            GameObject projectileClone = Instantiate(projectile, tf.position, tf.rotation) as GameObject;
            if (projectileClone != null) // Check if projectileClone exists
            {
                Destroy(projectileClone, projectileDuration); // Destroy projectile after projectileDuration
            }
        }
    }
}
