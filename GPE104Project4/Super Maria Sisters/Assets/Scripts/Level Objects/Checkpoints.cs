﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script attached to checkpoints, saves the last checkpoint hit by player and used when player dies and is revived
public class Checkpoints : MonoBehaviour
{
    private GameManager gm; // Game Manager
    public int checkpointNumber; // Must set the checkpoint number in the inspector
    public float notificationTimer = 2;

    private void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>(); // Set Game Manager variable
    }

    // Check if player triggers the checkpoint
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == GameManager.instance.playerObject) // Ensure the object that triggers the collider is the player
        {
            gm.checkpointIndex = checkpointNumber; // Sets the checkpointIndex stored in the game manager to the current checkpoint number

            // Update player checkpoint
            gm.playerSpawn = gm.playerCheckpoints[gm.checkpointIndex];

            GameManager.instance.checkpointCanvas.SetActive(true); // Enables the checkpoint notification
            StartCoroutine("CheckpointNotification"); // Enables the coroutine for the checkpoint notification
        }
    }

    // Coroutine to turn off the checkpoint notification
    private IEnumerator CheckpointNotification()
    {
        while (true)
        {
            yield return new WaitForSeconds(notificationTimer);
            // Disables Checkpoint notification
            GameManager.instance.checkpointCanvas.SetActive(false);
        }
    }
}
