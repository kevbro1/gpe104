﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Script run when the player reaches the treasure chest
public class TreasureChest : MonoBehaviour
{
    // When the player triggers the treasure chest collider, load the victory screen
    void OnTriggerEnter2D(Collider2D other)
    {
        SceneManager.LoadScene(2); // Loads the victory screen
    }
}
