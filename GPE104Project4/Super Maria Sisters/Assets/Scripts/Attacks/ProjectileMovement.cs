﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is attached to a projectile and dictates its movement
public class ProjectileMovement : MonoBehaviour
{
    private Transform tf; // Transform component variable
    public float speed = 0.5f; // Movement Speed variable

    void Start()
    {
        tf = GetComponent<Transform>(); // Transform component
    }

    void Update()
    {
        // Move towards target, used "right" because of how the sprite is oriented by default
        tf.Translate((Vector3.up * speed), Space.Self);
    }
}
