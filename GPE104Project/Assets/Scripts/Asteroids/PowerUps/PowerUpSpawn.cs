﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawn : MonoBehaviour
{
    private List<Transform> spawnList; // List of all power up spawn points, set list in Unity
    private List<GameObject> powerUpList; // List of power ups, set list in Unity
    private GameObject randomPowerUp; // Random enemy object
    private Transform spawnPoint; // Transform component for spawnPoint
    public float spawnSpeed = 2; // Allows setting of Spawn Speed in Unity

    // Start is called before the first frame update
    void Start()
    {
        spawnList = GameManager.instance.powerUpSpawnList; // The spawnList is taken from the GameManager.
        powerUpList = GameManager.instance.powerUpList; // The powerUpList is taken from the GameManager.
        StartCoroutine("SpawnPowerUp"); // Begin coroutine for SpawnPowerUps
    }
    private IEnumerator SpawnPowerUp()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnSpeed); // Timer
            // Check that activePowerUps does not exceed maxPowerUps
            if (GameManager.instance.maxPowerUps > GameManager.instance.activePowerUps)
            {
                spawnPoint = spawnList[Random.Range(0, spawnList.Count)]; // Random spawnPoint from list
                randomPowerUp = powerUpList[Random.Range(0, powerUpList.Count)]; // Random powerUp from list
                // Create powerUpClone instance
                GameObject powerUpClone = Instantiate(randomPowerUp, spawnPoint.position, spawnPoint.rotation) as GameObject;
                GameManager.instance.activePowerUps++; // Increase activePowerUps by one
            }
        }
    }
}
