﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enemy Ship movement, heat seeking on player
public class EnemyShipRush : MonoBehaviour
{
    private Transform tf; // Transform component of enemyShip
    private GameObject player; // GameObject for player
    public float shipSpeed = 0.3f; // Speed of rush attack
    public float rotationSpeed = 2.0f; // Speed of rotation
    private Transform p; // Transform component for player

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Get the Transform Component of enemyShip
        player = GameObject.FindWithTag("Player"); // Set player to GameObject with "player" tag (Must tag the player in Unity)
        if (player != null) // If player is not null then...
        {
            p = player.GetComponent<Transform>(); // Get transform component of player
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (p != null)
        {
            Vector3 targetVector = p.position - tf.position; // Calculate Vector between enemyShip and player
            float shipAngle = Mathf.Atan2(targetVector.x, targetVector.y) * Mathf.Rad2Deg; // Calculate the angle between x and y axis of targetVector
            // Set shipRotation based on shipAngle and Vector 3 axis ("back" based on enemyShip orientation)
            Quaternion shipRotation = Quaternion.AngleAxis(shipAngle, Vector3.back); 
            // Slerp between current rotation angle and desired rotation "shipRotation" and apply rotationSpeed
            transform.rotation = Quaternion.Slerp(transform.rotation, shipRotation, Time.deltaTime * rotationSpeed);
            tf.position = Vector3.MoveTowards(tf.position, p.position, shipSpeed); // Move the enemy ship towards the player. "Heat Seeking"
        }

    }
}
