﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Source of the tornado attack, on child of player
public class TornadoSource : MonoBehaviour
{
    public GameObject tornado; // Set the projectile in the inspector
    private PlayerData playerData;
    private Transform tf;
    void Start()
    {
        tf = GetComponent<Transform>(); // Get transform component
        playerData = GameManager.instance.playerData;
    }

    public void LaunchTornado()
    {
        if (playerData.tornadoManaCost < playerData.currentMana) // Check if the player has enough mana
        {
            // Creates a projectile instance with proper orientation
            GameObject tornadoClone = Instantiate(tornado, tf.position, tf.rotation) as GameObject;

            // Plays tornado sound effect when launched 
            GameManager.instance.soundManager.SoundTornado();

            if (tornadoClone != null) // Check if projectileClone exists
            {
                Destroy(tornadoClone, playerData.tornadoDuration); // Destroy projectile after projectileDuration
                playerData.currentMana -= playerData.tornadoManaCost; // Deduct the mana cost
            }
        }
    }
}
