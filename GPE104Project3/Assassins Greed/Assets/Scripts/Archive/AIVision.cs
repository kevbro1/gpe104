﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIVision : MonoBehaviour
{
    private GameObject player;
    private void Start()
    {
        player = GameObject.FindWithTag("Player"); // Set player to GameObject with "player" tag (Must tag the player in Unity)
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
            Debug.Log("Player in view");
    }
    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
            Debug.Log("Player out of view");
    }
}
