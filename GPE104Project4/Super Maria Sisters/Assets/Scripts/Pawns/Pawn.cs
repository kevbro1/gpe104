﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Pawn parent
public abstract class Pawn : MonoBehaviour
{
    [HideInInspector] public Transform tf; // Transform component
    [HideInInspector] public Rigidbody2D rb2d; // Rigidbody 2D component
    [HideInInspector] public SpriteRenderer sr; // Sprite Renderer component
    [HideInInspector] public Animator ar; // Animator component
    [HideInInspector] public SoundManager sm; // Sound Manager component
    [HideInInspector] public PlayerController playerController; // Player controller
    [HideInInspector] public GameManager gm; // Game Manager
    [HideInInspector] public ProjectileLaunch proj; // Projectile launch component
    [HideInInspector] public Transform projtf; // Projectile transform component (source of projectile launch)

    public void FlipPawn()
    {
        sr.flipX = rb2d.velocity.x < 0; // Flip sprite renderer component to make sprite face the direction it is moving

        if (this.gameObject == GameManager.instance.playerObject)
        {
            if (playerController.inputHorizontal > 0) // Check if inputHorizontal is positive to set projectile direction to right
            {
                projtf.transform.localEulerAngles = new Vector3(0, 0, -90);
            }
            if (playerController.inputHorizontal < 0) // Check if inputHorizontal is negative to set projectile direction to left
            {
                projtf.transform.localEulerAngles = new Vector3(0, 0, 90);
            }
        }
    }
}
