﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Collision script to destroy ENEMIES and ASTEROIDS, deducts one enemyCount
public class CollisionDestroy : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D otherObject)
    {
        Destroy(this.gameObject); // Destroy current object on collision
    }
    private void OnDestroy()
    {
        GameManager.instance.enemyCount--; // Deduct one from enemyCount when object is destroyed
    }
}

