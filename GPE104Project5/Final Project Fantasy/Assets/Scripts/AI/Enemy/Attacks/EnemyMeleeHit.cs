﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enemy basic melee attack script
public class EnemyMeleeHit : MonoBehaviour
{
    public float meleePower = 10; // Damage dealt

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player")) // Checks if other is player
        {
            GameManager.instance.playerData.currentHealth -= meleePower;
            //other.gameObject.GetComponent<Rigidbody2D>().AddForce(-transform.up * enemyPawn.meleeForce); // Add physics to attacks
        }
    }
}
