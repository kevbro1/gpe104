﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Basic Game Controls
public class GameControls : MonoBehaviour
{
    private Transform tf; // Transform component variable
    private SpriteMovement sm; // Transform component variable
    public GameObject currentObject; // GameObject variable

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Get transform component from object
        sm = GetComponent<SpriteMovement>(); // Get SpriteMovement component from object
    }

    // Update is called once per frame
    void Update()
    {
        // Set currentObject to Inactive when Q key is pressed
        if (Input.GetKeyDown(KeyCode.Q))
        {
            currentObject.SetActive(false); // Current object set to inactive
        }
        // Quit game when escape key is pressed
        if (Input.GetButtonDown("quit"))
        {
            Application.Quit();
        }
        // Press P key to toggle movement component
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (sm != null) // If SpriteMovement component is not null then...
            {
                // Toggles the component SpriteMovement on and off by setting it to whatever it is not currently set to
                sm.enabled = !sm.enabled;
            } 
        }
    }
}
