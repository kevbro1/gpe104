﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public PlayerPawn playerPawn;
    public PlayerData playerData;
    public float inputHorizontal; // Holds the horizontal input
    public float inputVertical; // Holds the vertical input

    void FixedUpdate()
    {
        // Detect horizontal movement for strafing
        if (Input.GetButton("Horizontal") && !Input.GetButton("Run"))
        {
            inputHorizontal = Input.GetAxis("Horizontal");
            if (inputHorizontal > 0)
            {
                playerPawn.Right();
            }
            if (inputHorizontal < 0)
            {
                playerPawn.Left();
            }
        }
        // Detect vertical movement for walking
        if (Input.GetButton("Vertical") && !Input.GetButton("Run"))
        {
            inputVertical = Input.GetAxis("Vertical");
            if (inputVertical > 0)
            {
                playerPawn.Forward();
            }
            if (inputVertical < 0)
            {
                playerPawn.Back();
            }
        }
        // Detect input for fast strafing
        if (Input.GetButton("Horizontal") && Input.GetButton("Run"))
        {
            inputHorizontal = Input.GetAxis("Horizontal");
            if (inputHorizontal > 0)
            {
                playerPawn.RunRight();
            }
            if (inputHorizontal < 0)
            {
                playerPawn.RunLeft();
            }
        }
        // Detect input for running and back peddling
        if (Input.GetButton("Vertical") && Input.GetButton("Run"))
        {
            inputVertical = Input.GetAxis("Vertical");
            if (inputVertical > 0)
            {
                playerPawn.RunForward();
            }
            if (inputVertical < 0)
            {
                playerPawn.RunBack();
            }
        }
        // Detect no movement input
        if (!Input.GetButton("Vertical") && !Input.GetButton("Horizontal"))
        {
            playerPawn.Standing();
        }
    }
    void Update()
    {
        // Check for button inputs

        // Left mouse input for melee attack
        if (Input.GetButtonDown("LeftMouse"))
        {
            playerPawn.MeleeAttack();
        }
        // Right mouse input for blocking
        if (Input.GetButton("RightMouse"))
        {
            playerPawn.Block();
        }
        // "1" for skill one
        if (Input.GetButtonDown("Skill1"))
        {
            playerPawn.Skill1();
        }
        // "2" for skill two
        if (Input.GetButtonDown("Skill2"))
        {
            playerPawn.Skill2();
        }
        // "3" for skill three
        if (Input.GetButtonDown("Skill3"))
        {
            playerPawn.Skill3();
        }
        // "4" for skill four
        if (Input.GetButtonDown("Skill4"))
        {
            playerPawn.Skill4();
        }

        // Update the direction of the mouse
        playerPawn.FaceMouseDir();
    }
}
