﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enemy pawn parent
public abstract class EnemyPawn : AIPawn
{
    private Transform ttf; // Target transform component
    public Transform tf; // This objects transform component
    public float fieldOfView; // Set Field of View in Unity, default 55 degrees
    public float maxViewDistance; // Max view distance of the object, can be set in Unity
    public float angleToTarget; // Angle to the target used in raycast
    public float damageDelay; // Seconds of delay between contact damage
    public bool canDamage = true; // Bool used to delay damage after contact with player
    public float contactDamage; // Damage done if contact is made with player
    public GameObject randomLoot; // Used to temporarily hold random loot object
    public float rotationSpeed; // Rotation speed
    public float rotationAmount; // Angle of rotation used during "searching" state
    public float walkSpeed; // Sets walk speed for alert function
    public float wanderSpeed; // Sets wander speed for enemies
    private int randomSpot; // Picks a random wanderSpot for the guard to walk to
    private float waitTime; // Time the guard will wait at a wanderSpot
    public float startWaitTime; // Default amount of time that a guard will wait at a wanderSpot
    public float wanderSpotThreshold; // Distance from the wanderSpot the enemy pawn needs to be for it to "reach" that wanderSpot

    public override void Start()
    {
        base.Start(); // Grab base start method
        aiController = this.gameObject.GetComponent<AIController>();
        tf = GetComponent<Transform>();
        waveManager = GameManager.instance.waveManager;
        player = GameManager.instance.player;
        p = player.GetComponent<Transform>();
        playerData = GameManager.instance.playerData;
    }
    public override void Update()
    {
        base.Update();
        if (currentHealth <= 0) // Check if the AI is dead
        {
            Death();
        }  
    }
    // Check if the player is visible by the AI
    public bool PlayerDetected(GameObject target)
    {
        // Get transform components
        ttf = target.GetComponent<Transform>(); 
        tf = this.GetComponent<Transform>();

        // Find the vector from current object to target
        Vector2 vectorToTarget = ttf.position - tf.position;

        // Find the distance between the two vectors in float to compare with maxViewDistance
        float targetDistance = Vector2.Distance(ttf.position, tf.position);

        // If x < 0 angle should be down since enemies start facing down
        angleToTarget = Vector2.Angle(vectorToTarget, -tf.up); 

        if (angleToTarget < fieldOfView  && targetDistance < maxViewDistance)
        {
            int wallLayer = LayerMask.NameToLayer("Environment"); // Add Environment layer to variable
            int playerLayer = LayerMask.NameToLayer("Player"); // Add Player layer to variable
            int layerMask = (1 << playerLayer) | (1 << wallLayer); // Create layermask
            
            // RaycastHit from object to target, maxview distance, and only affects objects in layermask
            RaycastHit2D hit = Physics2D.Raycast(tf.position, vectorToTarget, maxViewDistance, layerMask);
            
            if (hit.collider.CompareTag("Player")) // If hit is player then...
            {
                return true;
            }
        }
        return false;
    }
    // Method to rotate AI towards the player
    public void RotateTowardsPlayer()
    {
        tf = this.GetComponent<Transform>();
        p = GameManager.instance.player.GetComponent<Transform>();
        Vector2 targetVector = p.position - tf.position; // Calculate Vector between AI and player
        float angle = Mathf.Atan2(targetVector.y, targetVector.x) * Mathf.Rad2Deg; // Calculate the angle between x and y axis of targetVector
        // Set rotation based on angle and Vector 3 axis
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        // Slerp between current rotation angle and desired rotation
        tf.rotation = Quaternion.Slerp(tf.rotation, rotation, Time.deltaTime * rotationSpeed);
    }
    // Death script run on AI death
    public void Death()
    {
        StartCoroutine(DeathRoutine()); // AI coroutine, see below
    }
    // AI wandering around arena. Is not aggressive
    public void Wandering()
    {
        // Move the AI towards a random "wanderSpot" at the wanderSpeed
        transform.position = Vector2.MoveTowards(transform.position, waveManager.wanderSpots[randomSpot].position, wanderSpeed * Time.deltaTime);

        // If the distance between the AI and wander spot is less than the patrol spot threshold
        if (Vector2.Distance(transform.position, waveManager.wanderSpots[randomSpot].position) < wanderSpotThreshold)
        {
            if (waitTime <= 0) // Waits for the waitTime amount to be zero before moving again
            {
                randomSpot = Random.Range(0, waveManager.wanderSpots.Count);
                waitTime = startWaitTime;
            }
            else // Decrement waitTime
            {
                waitTime -= Time.deltaTime;
            }
        }
    }
    // AI searching coroutine, see below
    public void Searching()
    {
        StartCoroutine(LoopRotation(rotationAmount));
    }
    // Coroutine run afer an enemy pawn dies
    public IEnumerator DeathRoutine()
    {
        yield return new WaitForSeconds(1); // 1 Second delay
        waveManager = GameManager.instance.waveManager; 

        // Reduce concurrent enemies to make room for more enemy spawns
        waveManager.concurrentEnemies--;

        // Increase body count to determine when wave is over
        waveManager.bodyCount++;
        Destroy(this.gameObject);

        // Loot drop
        randomLoot = waveManager.lootList[Random.Range(0, waveManager.lootList.Count)]; // Random loot from list
        // Create loot clone instance
        GameObject lootClone = Instantiate(randomLoot, tf.position, tf.rotation) as GameObject;
    }
    // Coroutine for enemy to rotate to look for player during "searching" state
    IEnumerator LoopRotation(float angle)
    {
        float rot = 0f;
        float dir = 1f;
        while (true)
        {
            // Loop that rotates enemy pawn based on rotation speed up to a fixed angle
            while (rot < angle)
            {
                float step = Time.deltaTime * rotationSpeed;
                transform.Rotate(new Vector3(0, 0, 12) * step * dir);
                yield return null;
            }
            rot = 0f;
            dir *= -1f;
        }
    }
}
