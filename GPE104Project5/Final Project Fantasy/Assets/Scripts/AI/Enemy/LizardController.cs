﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lizard controller
public class LizardController : EnemyController
{
    public LizardPawn lizardPawn;
    void Start()
    {
        enemyPawn = this.GetComponent<LizardPawn>(); // Make sure to set this to the correct enemy child pawn
        lizardPawn = this.GetComponent<LizardPawn>(); // Used for attack functions
        player = GameManager.instance.player; // Get reference to player, used in parent methods
        aiState = "wandering"; // AI state set to default for lizard
    }
    // Run lizard attack method
    public override void DoAttackPlayer()
    {
        lizardPawn.RotateTowardsPlayer(); // Rotate towards player
        lizardPawn.AttackPlayer(); // Run attack script
    }
}
