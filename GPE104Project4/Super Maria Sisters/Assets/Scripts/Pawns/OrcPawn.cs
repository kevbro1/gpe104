﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Orc Pawn script
public class OrcPawn : EnemyPawn
{
    public float attackSpeed = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Transform component
        rb2d = GetComponent<Rigidbody2D>(); // Rigidbody component
        sm = GameManager.instance.soundManager; // Sound Manager component attached to main camera
        p = GameManager.instance.playerObject.GetComponent<Transform>(); // player transform component
        playerPawn = GameManager.instance.playerObject.GetComponent<PlayerPawn>(); // Player pawn component

        // COMPONENTS THAT MIGHT BE USED IN THE FUTURE
        //sr = GetComponent<SpriteRenderer>(); // Sprite Renderer component
        //ar = GetComponent<Animator>(); // Animator component
        //gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>(); // Game Manager component on Game Manager object
        //aiController = GetComponent<OrcController>(); // AI controller component
    }

    // Method for the orc's charge attack
    public void ChargeAttack()
    {
        tf.position = Vector3.MoveTowards(tf.position, p.position, attackSpeed); // Move the enemy ship towards the player. "Heat Seeking"
        sm.SoundOrcYell(); // Play the orc yell sound
    }

    void FixedUpdate()
    {
        rb2d.velocity = new Vector2(moveSpeed, rb2d.velocity.y); // Orc patrol movement
    }

    // Uses polymorphism to remove player death when the orc charges at the player. Orc will suicide throw themselves and player off cliffs.
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("PlayerAttack")) // Collision with player attack
        {
            Death(); // Run Death method
        }
    }
}
