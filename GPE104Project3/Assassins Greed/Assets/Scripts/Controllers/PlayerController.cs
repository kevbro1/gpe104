﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Basic movement inputs for player
public class PlayerController : Controller
{
    // Start is called before the first frame update
    void Start()
    {
        // Set playerPawn to be controlled by PlayerController
        playerPawn = GameManager.instance.playerObject.GetComponent<PlayerPawn>(); 
    }

    // Update is called once per frame
    void Update()
    {
        // Move up when W key is pressed, also NoSneak called
        if (Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.LeftShift))
        {
            playerPawn.MoveUp();
            playerPawn.NoSneak();
        }
        // Move down when S key is pressed, also NoSneak called
        if (Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.LeftShift))
        {
            playerPawn.MoveDown();
            playerPawn.NoSneak();
        }
        // Move Left when A key is pressed, also NoSneak called
        if (Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.LeftShift))
        {
            playerPawn.MoveLeft();
            playerPawn.NoSneak();
        }
        // Move right when D key is pressed, also NoSneak called
        if (Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.LeftShift))
        {
            playerPawn.MoveRight();
            playerPawn.NoSneak();
        }
        // Sneak up when W and Shift are pressed, also Sneak called
        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift))
        {
            playerPawn.SneakUp();
            playerPawn.Sneak();
        }
        // Sneak down when S and Shift are pressed, also Sneak called
        if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.LeftShift))
        {
            playerPawn.SneakDown();
            playerPawn.Sneak();
        }
        // Sneak left when A and Shift are pressed, also Sneak called
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.LeftShift))
        { 
            playerPawn.SneakLeft();
            playerPawn.Sneak();
        }
        // Sneak right when D and Shift are pressed, also Sneak called
        if (Input.GetKey(KeyCode.D)&& Input.GetKey(KeyCode.LeftShift))
        {
            playerPawn.SneakRight();
            playerPawn.Sneak();
        }
    }
}
