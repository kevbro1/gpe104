﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Pickup ammo for fireballs
public class AmmoPickup : MonoBehaviour
{
    private SoundManager sm; // Sound manager
    private void Start()
    {
        sm = GameManager.instance.soundManager; // Set sound manager variable
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (GameManager.instance.fireballCount <= GameManager.instance.fireballMax) // Check that the player does not have max ammo
        {
            sm.SoundPowerUp(); // Play the powerup sound
            Destroy(this.gameObject); // Destroy current object on collision
            GameManager.instance.fireballCount = 5; // Adds one to fireball count
        }
    }
}
