﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Kills all enemy objects and triggers on exit, used to make sure enemies that might escape arena are counted towards kill count
public class AllDeathVolume : MonoBehaviour
{
    public WaveManager waveManager;
    private void Start()
    {
        waveManager = GameManager.instance.waveManager; // Reference to wave manager
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Enemy")) // If the target is an enemy run the death method
        {
            other.GetComponent<EnemyPawn>().Death();
        }
    }
}
