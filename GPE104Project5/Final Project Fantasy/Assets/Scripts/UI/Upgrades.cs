﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Upgrade menu
public class Upgrades : MonoBehaviour
{
    // Upgrade text variables
    public PlayerData playerData;
    public Text maxHealth;
    public Text maxStamina;
    public Text maxMana;
    public Text maxHealthLevel;
    public Text maxStaminaLevel;
    public Text maxManaLevel;
    public Text maxHealthUpgradeText;
    public Text maxStaminaUpgradeText;
    public Text maxManaUpgradeText;
    public Text maxHealthCost;
    public Text maxStaminaCost;
    public Text maxManaCost;
    public float maxHealthUpgrade;
    public float maxStaminaUpgrade;
    public float maxManaUpgrade;
    public float baseMaxHealthUpgrade = 100;
    public float baseMaxStaminaUpgrade = 100;
    public float baseMaxManaUpgrade = 100;
    public int costMaxHealth;
    public int costMaxStamina;
    public int costMaxMana;
    public int baseCostMaxHealth = 500;
    public int baseCostMaxStamina = 500;
    public int baseCostMaxMana = 500;

    private void Update()
    {
        // Display current upgrade level
        maxHealthLevel.text = playerData.currentHealthLevel.ToString();
        maxStaminaLevel.text = playerData.currentStaminaLevel.ToString();
        maxManaLevel.text = playerData.currentManaLevel.ToString();
        // Display current raw values
        maxHealth.text = playerData.maxHealth.ToString();
        maxStamina.text = playerData.maxStamina.ToString();
        maxMana.text = playerData.maxMana.ToString();
        // Display upgraded values
        maxHealthUpgradeText.text = maxHealthUpgrade.ToString();
        maxStaminaUpgradeText.text = maxStaminaUpgrade.ToString();
        maxManaUpgradeText.text = maxManaUpgrade.ToString();
        // Display cost
        maxHealthCost.text = costMaxHealth.ToString();
        maxStaminaCost.text = costMaxStamina.ToString();
        maxManaCost.text = costMaxMana.ToString();
        // Update Upgrades and cost displays
        DisplayUpgrades();
        DisplayCosts();
    }
    // Close the upgrade menu
    public void CloseUpgrades()
    {
        GameManager.instance.waveManager.waveState = "beginWave";
    }
    // Display preview of the upgraded stats
    public void DisplayUpgrades()
    {
        maxHealthUpgrade = (playerData.currentHealthLevel * baseMaxHealthUpgrade) + playerData.maxHealth;
        maxStaminaUpgrade = (playerData.currentStaminaLevel * baseMaxStaminaUpgrade) + playerData.maxStamina;
        maxManaUpgrade = (playerData.currentManaLevel * baseMaxManaUpgrade) + playerData.maxMana;

    }
    // Display the cost for all upgrades
    public void DisplayCosts()
    {
        costMaxHealth = baseCostMaxHealth * playerData.currentHealthLevel;
        costMaxStamina = baseCostMaxStamina * playerData.currentStaminaLevel;
        costMaxMana = baseCostMaxMana * playerData.currentManaLevel;
    }
    // Upgrade max health method
    public void UpgradeMaxHealth()
    {
        if (playerData.currentHealthLevel < playerData.maxHealthLevel)
        {
            if (costMaxHealth <= playerData.coins)
            {
                // Plays power up sound when upgrade is purchased
                GameManager.instance.soundManager.SoundPowerUp();

                playerData.coins -= costMaxHealth;
                playerData.maxHealth = maxHealthUpgrade;
                playerData.currentHealthLevel++;
                playerData.currentHealth = playerData.maxHealth;
            }
        }
    }
    // Upgrade max stamina method
    public void UpgradeMaxStamina()
    {
        if (playerData.currentStaminaLevel < playerData.maxStaminaLevel)
        {
            if (costMaxStamina <= playerData.coins)
            {
                // Plays power up sound when upgrade is purchased
                GameManager.instance.soundManager.SoundPowerUp();

                playerData.coins -= costMaxStamina;
                playerData.maxStamina = maxStaminaUpgrade;
                playerData.currentStaminaLevel++;
                playerData.currentStamina = playerData.maxStamina;
            }
        }
    }
    // Upgrade max mana method
    public void UpgradeMaxMana()
    {
        if (playerData.currentManaLevel < playerData.maxManaLevel)
        {
            if (costMaxMana <= playerData.coins)
            {
                // Plays power up sound when upgrade is purchased
                GameManager.instance.soundManager.SoundPowerUp();

                playerData.coins -= costMaxMana;
                playerData.maxMana = maxManaUpgrade;
                playerData.currentManaLevel++;
                playerData.currentMana = playerData.maxMana;
            }
        }
    }
}

