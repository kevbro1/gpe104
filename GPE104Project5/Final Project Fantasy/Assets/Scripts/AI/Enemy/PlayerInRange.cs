﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Check to see if the player is in range, used for "searching" AI state
public class PlayerInRange : MonoBehaviour
{
    private GameObject senses; // Game object for sense object that hold sense collider
    [HideInInspector] public bool inRange; // Is the player inRange of sensory information

    public void OnTriggerEnter2D(Collider2D other) // OnTriggerEnter for intersecting sense collider
    {
        if (other.gameObject.CompareTag("Player")) // If game object is tagged with senses, change to true
        {
            inRange = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)  // OnTriggerExit for intersecting sense collider
    {
        if (other.gameObject.CompareTag("Player")) // If game object is tagged with senses, change to false
        {
            inRange = false;
        }
    }
}
