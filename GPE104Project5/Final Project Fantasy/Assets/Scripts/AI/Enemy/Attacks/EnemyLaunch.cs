﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enemy launches a projectile. This is the source of the projectile
public class EnemyLaunch : MonoBehaviour
{
    public GameObject projectile; // Set the projectile in the inspector
    public float projectileDuration; // Duration that projectile exists
    public float attackDelay = 5; // Delay time between attacks
    public float lastAttack = 0; // Used in loop for attack delay
    private Transform tf;

    void Start()
    {
        tf = GetComponent<Transform>(); // Get transform component
    }

    public void LaunchProjectile()
    {
        if (Time.time > lastAttack + (attackDelay * Time.deltaTime)) // Delays the attack using Time.time as a reference
        {
            // Creates a projectile instance with proper orientation
            GameObject projectileClone = Instantiate(projectile, tf.position, tf.rotation) as GameObject;
            if (projectileClone != null) // Check if projectileClone exists
            {
                Destroy(projectileClone, projectileDuration); // Destroy projectile after projectileDuration
            }
        }
    }
}
