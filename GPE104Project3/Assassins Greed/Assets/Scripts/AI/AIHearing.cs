﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// AI hearing script used to detect player and AI sound collider collision also has raycast for last known location of sound
public class AIHearing : MonoBehaviour
{
    private GameObject sound; // sound radius game object
    private GameObject player; // player game object
    private Transform tf; // transform component
    private Transform ttf; // target (player) transform component
    public float maxHearDistance = 5.0f; // AI max hearing distance
    [HideInInspector] public bool canHear; // Used to activate the sound raycast
    [HideInInspector] public Vector3 lastSoundLocation; // lastSoundLocation vector set by raycast.point

    // Start is called before the first frame update
    private void Start()
    {
        sound = GameObject.FindWithTag("Sound"); // Set sound to GameObject with "sound" tag (Must tag the sound in Unity)
        player = GameManager.instance.playerObject; // Get player object from GameManager
        ttf = player.GetComponent<Transform>(); // Get target transform component
        tf = GetComponent<Transform>(); // transform component
    }

    public void OnTriggerEnter2D(Collider2D other) // OnTriggerEnter for intersecting sound collider
    {
        if (other.gameObject == sound) // If game object is tagged with sound, return true
        {
            canHear = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)  // OnTriggerExit for intersecting sound collider
    {
        if (other.gameObject == sound) // If game object is tagged with sound, return false
        {
            canHear = false;
        }
    }
    private void Update()
    {
        if (canHear == true)
        {
            // Find the vector from current object to target
            Vector2 vectorToSound = ttf.position - tf.position;

            //RaycastHit from object to target, max hearing distance, and only affects objects in layermask
            RaycastHit2D hit = Physics2D.Raycast(tf.position, vectorToSound);
            
            lastSoundLocation = hit.point; // Save ray hit point as lastSoundLocation
            GameManager.instance.lastSoundLocation = lastSoundLocation; // Set lastSoundLocation in GameManager

            Debug.DrawRay(tf.position, vectorToSound, Color.blue, maxHearDistance); // Draw rays
        }
    }
}
