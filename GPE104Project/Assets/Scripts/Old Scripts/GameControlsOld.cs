﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Basic Game Controls
public class GameControlsOld : MonoBehaviour
{
    private Transform tf; // Transform component variable
    public GameObject currentObject; // GameObject variable

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Get transform component from object
    }

    // Update is called once per frame
    void Update()
    {
        // Reset to vector (0, 0, 0) when spacebar is pressed
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 myVector = new Vector3(0, 0, 10); // myVector zeros X and Y axis. Z axis set to 10 so sprite does not disappear
            tf.position = myVector; // tf position set to myVector
        }
        // Set currentObject to Inactive when Q key is pressed
        if (Input.GetKeyDown(KeyCode.Q))
        {
            currentObject.SetActive(false); // Current object set to inactive
        }
        // Quit game when escape key is pressed
        if (Input.GetButtonDown("quit"))
        {
            Application.Quit();
        }
        // Press P key to toggle movement component
        if (Input.GetKeyDown(KeyCode.P))
        {
            // Toggles the component SpriteMovement on and off by setting it to whatever it is not currently set to
            GetComponent<SpriteMovement>().enabled = !GetComponent<SpriteMovement>().enabled;
        }
    }
}