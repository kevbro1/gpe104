﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    // Allow the sounds to be set in the inspector
    public GameObject player; // Player object, set when player object is created
    public AudioSource asPlayer; // Player audio source, set when the player object is created, used for player generated sounds
    public AudioSource asCamera;
    public AudioClip fireball;
    public AudioClip pickup;
    public AudioClip bounce;
    public AudioClip evilLaugh;
    public AudioClip powerUp;
    public AudioClip woohoo;
    public AudioClip yell;
    public AudioClip dragonRoar;
    public AudioClip tornado;

    private void Start()
    {
        asCamera = Camera.main.GetComponent<AudioSource>(); // Set audio source for the main camera, used for external sounds
    }

    // The following methods play sounds at the player audio source
    public void SoundFireball()
    {
        asCamera.clip = fireball;
        asCamera.Play();
    }
    public void SoundPickup()
    {
        asPlayer.clip = pickup;
        asPlayer.Play();
    }
    public void SoundBounce()
    {
        asPlayer.clip = bounce;
        asPlayer.Play();
    }
    public void SoundEvilLaugh()
    {
        asCamera.clip = evilLaugh;
        asCamera.Play();
    }
    public void SoundPowerUp()
    {
        asPlayer.clip = powerUp;
        asPlayer.Play();
    }
    public void SoundWoohoo()
    {
        asCamera.clip = woohoo;
        asCamera.Play();
    }
    public void SoundYell()
    {
        asCamera.clip = yell;
        asCamera.Play();
    }
    public void SoundDragonRoar()
    {
        asCamera.clip = dragonRoar;
        asCamera.Play();
    }
    public void SoundTornado()
    {
        asPlayer.clip = tornado;
        asPlayer.Play();
    }
}
