﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Player pawn script
public class PlayerPawn : Pawn
{
    public float walkSpeed = 3; // Set walk speed
    public float runSpeed = 6; // Set run speed
    public float jumpHeight = 1; // Set jump height
    public int maxJumps = 2; // Set maximum number of jumps, reset when touching ground
    [HideInInspector] public int jumpCount = 0; // Current number of jumps
    private void Start()
    {
        tf = GetComponent<Transform>(); // Transform component
        rb2d = GetComponent<Rigidbody2D>(); // Rigidbody component
        playerController = GameObject.FindWithTag("GameController").GetComponent<PlayerController>(); // Player Controller component
        sr = GetComponent<SpriteRenderer>(); // Sprite Renderer component
        ar = GetComponent<Animator>(); // Animator component
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>(); // Game Manager component on Game Manager object
        sm = GameManager.instance.soundManager; // Sound Manager component
        proj = GetComponentInChildren<ProjectileLaunch>(); // Projectile Launch component attached to projectile launch object that is the child of the player
        projtf = transform.GetChild(0); // The transform component of the child of a pawn, index of "0" refers to the fireball source child of the player
    }
    // Stand method
    public void Stand()
    {
        ar.Play("PlayerStand"); // Player stand animation
    }
    // Walk method
    public void Walk()
    {
        ar.Play("PlayerWalk"); // Player walk animation
        rb2d.velocity = new Vector2(rb2d.velocity.y, (playerController.inputHorizontal * walkSpeed)); // Move left or right
        rb2d.velocity = new Vector2((playerController.inputHorizontal * walkSpeed), rb2d.velocity.x); // Not sure why I need this to prevent diagonal movement
    }
    // Run method
    public void Run()
    {
        ar.Play("PlayerRun"); // Player run animation
        rb2d.velocity = new Vector2(rb2d.velocity.y, (playerController.inputHorizontal * runSpeed)); // Move left or right
        rb2d.velocity = new Vector2((playerController.inputHorizontal * runSpeed), rb2d.velocity.x); // Not sure why I need this to prevent diagonal movement
    }
    // Jump method
    public void Jump()
    {
        ar.Play("PlayerJump"); // Player jump animation
        rb2d.AddForce(Vector2.up * jumpHeight); // Physics jump by AddForce
        sm.SoundBounce(); // Play bounce sound
    }
    // Attack method
    public void ProjectileAttack()
    {
        if (GameManager.instance.fireballCount > 0) // Check if the player has enough fireballs
        {
            proj.LaunchProjectile(); // Launch fireballs
            sm.SoundFireball(); // Play fireball sound
            GameManager.instance.fireballCount--; // Deduct one from fireball ammunition
        }
    }
    // player death method
    public void Death()
    {
        GameManager.instance.playerLives--; // Deduct one life
        if (GameManager.instance.playerLives == 0) // Check if player has lives remaining
        {
            GameManager.instance.gameState = "defeat"; // If no lives remaining then set game state to defeat
        }
        else // If the player has lives remaining...
        {
            tf.position = gm.playerSpawn.position; // Player position set to myVector
            sm.SoundOuch(); // play ouch sound
        }
    }
    void Update()
    {
        // Check if the pawn isGrounded
        RaycastHit2D hit = Physics2D.Raycast(tf.position, Vector2.down, 0.2f); // Creates a raycast below player pawn
        if (hit.collider != null) // Check if the hit is null
        {
            jumpCount = 0; // Sets jump count, used for double jump.
        }
    }
}
