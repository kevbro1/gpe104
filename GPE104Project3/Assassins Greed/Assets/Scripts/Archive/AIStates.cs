﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIStates : MonoBehaviour
{
    public string aiState = "patrolling";


    public void DoPatrol() // Run if the AI does not see or hear player
    {
        // Write patrol code. Random pathfinding in a fixed zone
    }
    public void DoAlert() // Run if AI hears but does not see player
    {
        // Write alerted code. Check the area sound came from. Do not run
    }
    public void DoPursuit() // Run if AI sees the player
    {
        // Write pursuit code. Chase after player.
    }
    public void ChangeState(string newState)
    {
        aiState = newState; // Change the AI's state
    }
    void Update()
    {
        if (aiState == "patrolling")
        {
            DoPatrol();
            // Write transition to alerted or pursuing state
        }
        else if (aiState == "alerted")
        {
            DoAlert();
            // Write transition to pursuing or patrolling state. Need timer to revert back to patrolling if not heard or seen.
        }
        else if (aiState == "pursuing")
        {
            DoPursuit();
            // Write transition to alerted or patrolling state. Need timer to revert back to patrolling if not heard or seen.
        }
    }
}
