﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Control asteroid movement, non-heat seeking
public class AsteroidMovement : MonoBehaviour
{
    private Transform tf; // Transform component of asteroid
    public GameObject player; // GameObject for player
    public float speed; // Speed of rush attack
    private Transform p; // Transform component for player
    private Vector3 direction; // Vector that holds the direction the asteroid will move in

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Get the Transform Component
        player = GameObject.FindWithTag("Player"); // Set player to GameObject with "player" tag (Must tag the player in Unity)
        if (player != null) // If player is not null then...
        {
            p = player.GetComponent<Transform>(); // Get transform component of player
            if (p != null) // If p is not null then...
            {
                direction = (tf.position - p.position).normalized; // Set direction of asteroid
            }  
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (direction != null) // Check that direction is not null
        {
            tf.position -= direction * speed * Time.deltaTime; // Move asteroid in direction of player
        }
    }
}
