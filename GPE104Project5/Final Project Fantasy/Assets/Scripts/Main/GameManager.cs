﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Static game objects and components
    public static GameManager instance;
    public GameObject player;
    public GameObject playerProto; // Set in inspector
    public SoundManager soundManager;
    public PlayerPawn playerPawn;
    public PlayerData playerData; 
    public WaveManager waveManager;
    public PlayerController playerController;
    public Transform playerSpawn;
    public Camera mainCamera;
    public Upgrades upgradeData;

    // Canvas
    public GameObject pauseMenu;
    public GameObject upgrade;
    public GameObject victory;
    public GameObject defeat;
    public HUD hud;


    public string gameState = "active"; // Game state for state machine

    private void Awake()
    {
        // Singleton
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        // Set all static variables at the start
        playerController = GameObject.FindWithTag("GameController").GetComponent<PlayerController>();
        hud = GameObject.FindWithTag("HUD").GetComponent<HUD>();
        pauseMenu = GameObject.FindWithTag("PauseMenu");
        upgrade = GameObject.FindWithTag("Upgrade");
        victory = GameObject.FindWithTag("Victory");
        defeat = GameObject.FindWithTag("Defeat");
        upgradeData = upgrade.GetComponent<Upgrades>();
        mainCamera = Camera.main;
        waveManager = GetComponent<WaveManager>();
        soundManager = GetComponent<SoundManager>();

        // Deactivate all the canvas menus
        pauseMenu.SetActive(false);
        upgrade.SetActive(false);
        victory.SetActive(false);
        defeat.SetActive(false);
        
    }

    void Update()
    {
        // Enable pauseMenu
        if (gameState == "pause") 
        {
            DoPause();
        }
        // Game in an active state, state subdivided in wave manager
        if (gameState == "active")
        {
            DoActive();
            // Transition to Pause
            if (Input.GetButton("Cancel"))
            {
                gameState = "pause";
            }
            // TEMPORARY TO TEST UPGRADE MENU
            if (Input.GetKeyDown(KeyCode.I))
            {
                gameState = "upgrade";
            }
        }
        // Return to main menu
        if (gameState == "menu") 
        {
            DoMainMenu();
        }
        // Quit the game
        if (gameState == "quit") 
        {
            DoQuit();
        }
    }
    // Game in active state
    public void DoActive()
    {
        // ***Load Wave Manager
        pauseMenu.SetActive(false);

        // Enable all AI movement
        Time.timeScale = 1;

        // Enable player controller
        playerController.GetComponent<PlayerController>().enabled = true;
    }
    // Game in paused state
    public void DoPause()
    {
        pauseMenu.SetActive(true);
        // Enable all AI movement
        Time.timeScale = 0;

        // Enable player controller
        playerController.GetComponent<PlayerController>().enabled = true;
    }
    // Game in main menu state
    public void DoMainMenu()
    {
        SceneManager.LoadScene(0);
    }
    // Quit the game
    public void DoQuit()
    {
        Application.Quit();
    }
    // Create an instance of the player object
    public void CreatePlayerObject()
    {
        player = Instantiate(playerProto, playerSpawn.position, playerSpawn.rotation) as GameObject;
    }
    // Variables to update when a new player is instantiated. A lot of these are in other scripts. This allows for fewer checks in update methods.
    public void UpdatePlayer()
    {
        playerData = player.GetComponent<PlayerData>();
        playerPawn = player.GetComponent<PlayerPawn>();
        playerController.playerPawn = GameManager.instance.playerPawn;
        playerController.playerData = GameManager.instance.playerData;
        waveManager.player = GameManager.instance.player;
        waveManager.playerData = GameManager.instance.playerData;
        waveManager.playerController = GameManager.instance.playerController;
        playerPawn.playerController = GameManager.instance.playerController;
        playerPawn.playerData = GameManager.instance.playerData;
        hud.playerData = GameManager.instance.playerData;
        hud.waveManager = GameManager.instance.waveManager;
        mainCamera.GetComponent<CameraFollow>().player = GameManager.instance.player;
        upgradeData.playerData = GameManager.instance.playerData;
        soundManager.player = GameManager.instance.player;
        soundManager.asPlayer = player.GetComponent<AudioSource>();
    }
}
