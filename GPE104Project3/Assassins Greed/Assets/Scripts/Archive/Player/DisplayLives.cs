﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Displays how many lives the player has. Attach component to camera.
public class DisplayLives : MonoBehaviour
{
    public Text lifeCount; // Life Count Text string. Set text object to update in Unity

    // Update is called once per frame
    void Update()
    { 
        // Print lives to screen. Typecast int to string
        //lifeCount.text = GameManager.instance.lives.ToString();
    }
}
