﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Custom mouse cursor script
public class MouseCursor : MonoBehaviour
{
    private void Start()
    {
        Cursor.visible = true; // Need to enable until bug is fixed where cursor is under canvas
    }
    // Update is called once per frame
    void Update()
    {
        Vector2 cursorPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = cursorPosition;
    }
}
