﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script used to spawn pickups for the player to collect
public class PickupSpawn : MonoBehaviour
{
    private List<Transform> spawnList; // List of all spawn points
    private List<GameObject> pickupList; // List of pickups
    private GameObject randomPickup; // Random pickup object
    private Transform spawnPoint; // Transform component for spawnPoint
    public float spawnSpeed = 1; // Set how quickly the pickups spawn

    void Start()
    {
        spawnList = GameManager.instance.pickupSpawnList; // The spawnList is taken from the GameManager.
        pickupList = GameManager.instance.pickupList; // The pickupList is taken from the GameManager.
        StartCoroutine("SpawnPickupEvent"); // Begin coroutine for SpawnPickupEvent
    }

    // Coroutine to spawn pickups on a timer
    private IEnumerator SpawnPickupEvent()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnSpeed); // Timer
            // Check that pickupCount does not exceed maxPickups
            if (GameManager.instance.maxPickups > GameManager.instance.pickupCount)
            {
                spawnPoint = spawnList[Random.Range(0, spawnList.Count)]; // Random spawnPoint from list
                randomPickup = pickupList[Random.Range(0, pickupList.Count)]; // Random ppickup from list
                GameManager.instance.pickupSpawnList.Remove(spawnPoint); // Remove spawn point from list when used
                // Create pickupClone instance
                GameObject pickupClone = Instantiate(randomPickup, spawnPoint.position, spawnPoint.rotation) as GameObject;
                GameManager.instance.pickupCount++; // Increase pickupCount by one
            }
        }
    }
}
