﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// PlayerPawn child of Pawn, includes player specific movement and Sneak/NoSneak functions
public class PlayerPawn : Pawn
{
    public float sneakSpeed = 0.02f; // Movement Speed variable
    public bool isSneak = false; // Variable determines whether player is sneaking, affects the sound they create.
    public float transparency = 0.4f;
    void Start()
    {
        tf = GetComponent<Transform>(); // Get the Transform Component
        sound = GameObject.FindWithTag("Sound"); // Find sound radius GameObject
    }

    public virtual void SneakUp() // Relative movement up (in local space)
    {
        tf.Translate((Vector3.up * sneakSpeed), Space.Self); 
    }
    public virtual void SneakDown() // Relative movement down (in local space)
    {
        tf.Translate((Vector3.down * sneakSpeed), Space.Self); 
    }
    public virtual void SneakLeft() // Relative movement left (in local space)
    {
        tf.Translate((Vector3.left * sneakSpeed), Space.Self); 
    }
    public virtual void SneakRight() // Relative movement right (in local space)
    {
        tf.Translate((Vector3.right * sneakSpeed), Space.Self); 
    }
    public virtual void NoSneak() // Sets player to not sneaking, used when moving and affects the sound the player creates
    {
        isSneak = false;
        sound.GetComponent<CircleCollider2D>().radius = 0.5f; // Reduce sound radius by 50%
        GameManager.instance.playerObject.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f, 1.0f);
    }
    public virtual void Sneak() // Sets player to sneaking, used when sneaking and affects the sound the player creates, also increases transparency
    {
        isSneak = true;
        sound.GetComponent<CircleCollider2D>().radius = 0.25f; // Amplify sound radius by 50%
        GameManager.instance.playerObject.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f, transparency); // Reduce alpha with transparency variable
    }
}
