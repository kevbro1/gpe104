﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Robot pawn script
public class RobotPawn : EnemyPawn
{
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>(); // Rigidbody component
        trip = GetComponentInChildren<TripleFire>(); // Projectile Launch component attached to projectile launch object that is the child of the robot
        triptf = transform.GetChild(0); // The transform component of the child of a pawn, index of "0" refers to the laser source child of the robot
        playerPawn = GameManager.instance.playerObject.GetComponent<PlayerPawn>(); // PlayerPawn component of player

        // COMPONENTS THAT MIGHT BE USED IN THE FUTURE
        //tf = GetComponent<Transform>(); // Transform component
        //sr = GetComponent<SpriteRenderer>(); // Sprite Renderer component
        //ar = GetComponent<Animator>(); // Animator component
        //gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>(); // Game Manager component on Game Manager object
        //aiController = GetComponent<RobotController>(); // aiController set to RobotController
        
    }
    // Robot triple attack method
    public void TripleFireAttack()
    {
        // Method used to fire three fireballs towards the player's direction
        if (direction.x > 0) // Check if inputHorizontal is positive to set projectile direction to right
        {
            triptf.transform.localEulerAngles = new Vector3(0, 0, -90);
        }
        if (direction.x < 0) // Check if inputHorizontal is negative to set projectile direction to left
        {
            triptf.transform.localEulerAngles = new Vector3(0, 0, -90);
        }
            trip.LaunchProjectile(); // Launch the triple projectile
    }
    void FixedUpdate()
    {
        rb2d.velocity = new Vector2(moveSpeed, rb2d.velocity.y); // Move pawn in direction it is facing
    }
}
