﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Holds all player stats and stat based calculations
public class PlayerData : MonoBehaviour
{
    // Player variables
    public float currentHealth = 100;
    public float currentMana = 100;
    public float currentStamina = 100;
    public float maxHealth = 100;
    public float maxMana = 100;
    public float maxStamina = 100;
    public float healthPercent;
    public float staminaPercent;
    public float manaPercent;
    public float staminaRegenRate = 5;
    public float healthRegenRate = 5;
    public float manaRegenRate = 5;
    public float staminaRegenAmount = 20;
    public float healthRegenAmount = 20;
    public float manaRegenAmount = 20;
    public float staminaReductAmount = 1;
    public int maxHealthLevel = 10;
    public int maxStaminaLevel = 10;
    public int maxManaLevel = 10;
    public int currentHealthLevel = 1;
    public int currentStaminaLevel = 1;
    public int currentManaLevel = 1;
    public int baseMaxHealthCost = 100;
    public int baseMaxStaminaCost = 100;
    public int baseMaxManaCost = 100;
    private bool isRegenStamina = false;
    private bool isRegenHealth = false;
    private bool isRegenMana = false; 
    public float attackAnimationDelay = 3.0f;
    public int coins = 1000;
    public float walkSpeed = 1;
    public float runSpeed = 1;
    public float rangedPower = 100;
    public float meleePower = 100;
    public float meleeForce = 10;

    // Blocking
    public float blockStaminaCost = 10;

    // Tornado 
    public float tornadoPower = 200;
    public float tornadoDuration = 3; // Duration that projectile exists
    public float tornadoManaCost = 50;
    public float tornadoMoveSpeed; // Sets tornado speed
    public float tornadoDelay = 3; // Delay time is how long the tornado will stay in one spot
    public float tornadoSpotThreshold = 0.5f; // Distance from the spot the tornado needs to be to register

    void Start()
    {
        // Reset main stats
        currentHealth = maxHealth;
        currentMana = maxMana;
        currentStamina = maxStamina;
    }

    void Update()
    {
        // Calculate percentages for main stats, used for UI
        healthPercent = currentHealth / maxHealth;
        staminaPercent = currentStamina / maxStamina;
        manaPercent = currentMana / maxMana;

        // Checks if stats are not the max and begins regen coroutines
        if (currentStamina != maxStamina && !isRegenStamina)
        {
            StartCoroutine("StaminaRegenTimer");
        }
        if (currentHealth != maxHealth && !isRegenHealth)
        {
            StartCoroutine("HealthRegenTimer");
        }
        if (currentMana != maxMana && !isRegenMana)
        {
            StartCoroutine("ManaRegenTimer");
        }
    }
    // Regen calculation methods
    public void StaminaRegen()
    {
        // No negatives and over max
        if (currentStamina < maxStamina && currentStamina >= 0)
        {
            currentStamina += staminaRegenAmount;
        }
        if (currentStamina > maxStamina)
        {
            currentStamina = maxStamina;
        }
    }
    public void HealthRegen()
    {
        // No negatives and over max
        if (currentHealth < maxHealth && currentHealth >= 0)
        {
            currentHealth += healthRegenAmount;
        }
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
    }
    public void ManaRegen()
    {
        // No negatives and over max
        if (currentMana < maxMana && currentHealth >= 0)
        {
            currentMana += manaRegenAmount;
        }
        if (currentMana > maxMana)
        {
            currentMana = maxMana;
        }
    }
    // Regen coroutines, to add a predetermined delay between regeneration
    private IEnumerator StaminaRegenTimer()
    {
        isRegenStamina = true; // bool used to initiate coroutine in update method only once
        while (currentStamina < maxStamina)
        {
            yield return new WaitForSecondsRealtime(staminaRegenRate);
            StaminaRegen();
        }
        isRegenStamina = false;
    }
    private IEnumerator HealthRegenTimer()
    {
        isRegenHealth = true;
        while (currentHealth < maxHealth)
        {
            yield return new WaitForSecondsRealtime(healthRegenRate);
            HealthRegen();
        }
        isRegenHealth = false;
    }
    private IEnumerator ManaRegenTimer()
    {
        isRegenMana = true;
        while (currentMana < maxMana)
        {
            yield return new WaitForSecondsRealtime(manaRegenRate);
            ManaRegen();
        }
        isRegenMana = false;
    }
}
