﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Camera follows the player with a slight delay
public class CameraFollow : MonoBehaviour
{
    // Camera script used to follow the player
    public GameObject player; // player object
    public float speed = 0.5f; // Sets the speed the camera follows behind player

    void Update()
    {
        float interpolate = speed * Time.deltaTime; // Calculate interpolate time based on speed
        Vector3 camera = this.transform.position; // Get the camera position
        camera.y = Mathf.Lerp(this.transform.position.y, player.transform.position.y, interpolate); // Lerp the camera's y position
        camera.x = Mathf.Lerp(this.transform.position.x, player.transform.position.x, interpolate); // Lerp the camera's x position
        this.transform.position = camera; // Set the new position of the camera
    }
}
