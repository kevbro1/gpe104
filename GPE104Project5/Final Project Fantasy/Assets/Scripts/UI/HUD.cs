﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Player HUD
public class HUD : MonoBehaviour
{
    public PlayerData playerData;
    public WaveManager waveManager;
    public Image healthBar;
    public Image staminaBar;
    public Image manaBar;
    public Text coinCount;
    public Text waveNumber;
    public Image enemyBar;
    void Start()
    {
        // Ensure all fill images are set properly
        healthBar.type = Image.Type.Filled;
        staminaBar.type = Image.Type.Filled;
        manaBar.type = Image.Type.Filled;
        enemyBar.type = Image.Type.Filled;
    }

    void Update()
    {
        // Update main stat bars
        healthBar.fillAmount = playerData.healthPercent;
        manaBar.fillAmount = playerData.manaPercent;
        staminaBar.fillAmount = playerData.staminaPercent;

        // Update coin, wave and enemies remaining
        coinCount.text = playerData.coins.ToString();
        waveNumber.text = waveManager.currentWave.ToString();
        enemyBar.fillAmount = waveManager.percentEnemiesRemaining;
    }
}
