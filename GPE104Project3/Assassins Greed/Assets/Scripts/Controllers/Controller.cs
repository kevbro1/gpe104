﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour
{
    [HideInInspector] public PlayerPawn playerPawn; // playerPawn component
    [HideInInspector] public GameObject guard; // Sets game object to guard
    [HideInInspector] public AISight sight; // Used for AIsight component
    [HideInInspector] public AIHearing hearing; // Used for AIhearing component
    [HideInInspector] public GuardPawn guardPawn; // Used for guardPawn component
    [HideInInspector] public AISenseRange senses; // Used for AISenseRange component
}
