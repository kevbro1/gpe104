﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script to handle the random tornado movement and damage
public class TornadoMovement : MonoBehaviour
{
    public PlayerData playerData;
    private int randomSpot; // Picks a random spot for the tornado to move to
    private float tornadoWaitTime; // Wait time tracks the number of seconds tornado will stay in one spot
    public List<Transform> tornadoSpots; // List of spots for the tornado to travel between
    void Start()
    {
        playerData = GameManager.instance.playerData;
    }

    // Update is called once per frame
    void Update()
    {
        randomSpot = Random.Range(0, tornadoSpots.Count); // Sets random spot
        // Move the AI towards a random "patrolSpot" at the patrolSpeed
        transform.position = Vector2.MoveTowards(transform.position, tornadoSpots[randomSpot].position, playerData.tornadoMoveSpeed * Time.deltaTime);

        // If the distance between the AI and patrolSpot is less than the patrol spot threshold
        if (Vector2.Distance(transform.position, tornadoSpots[randomSpot].position) < playerData.tornadoSpotThreshold)
        {
            if (tornadoWaitTime <= 0) // Waits for the waitTime amount to be zero before moving again
            {
                randomSpot = Random.Range(0, tornadoSpots.Count);
                tornadoWaitTime = playerData.tornadoDelay;
            }
            else // Decrement waitTime
            {
                tornadoWaitTime -= Time.deltaTime;
            }
        }
    }
    // Tornado attack collider
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy")) // Check if the other object is the enemy
        {
            other.gameObject.GetComponent<EnemyPawn>().currentHealth -= playerData.tornadoPower; // Apply damage to the enemy
            // ** Can add force to attacks
        }
    }
}
