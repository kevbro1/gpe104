﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Grants a life when player collides with power up and destroys power up
public class LifePowerUp : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D other)
    {
        Destroy(this.gameObject); // Destroy current object on collision
        //GameManager.instance.lives++; // Adds one life
    }
    private void OnDestroy()
    {
        //GameManager.instance.activePowerUps--; // Deduct one from activePowerUps when object is destroyed
    }
}
