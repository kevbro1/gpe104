﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Manages all the sounds used in the game
public class SoundManager : MonoBehaviour
{
    // Allow the sounds to be set in the inspector
    public AudioSource audioSource;
    public AudioClip fireball;
    public AudioClip laser;
    public AudioClip pickup;
    public AudioClip splash;
    public AudioClip bounce;
    public AudioClip evilLaugh;
    public AudioClip ouch;
    public AudioClip powerUp;
    public AudioClip pop;
    public AudioClip woohoo;
    public AudioClip goreSplat;
    public AudioClip orcYell;
    public AudioClip skullSound;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>(); // Get the AudioSource component
    }
    public void ToggleSound() // Method to toggle sound on and off
    {
        if (audioSource.isPlaying)
        {
            audioSource.Pause();
        }
        else
        {
            audioSource.Play();
        }
    }
    // The following functions run the respective sounds when called
    public void SoundFireball()
    {
        audioSource.clip = fireball;
        audioSource.Play();
    }
    public void SoundLaser()
    {
        audioSource.clip = laser;
        audioSource.Play();
    }
    public void SoundPickup()
    {
        audioSource.clip = pickup;
        audioSource.Play();
    }
    public void SoundSplash()
    {
        audioSource.clip = splash;
        audioSource.Play();
    }
    public void SoundBounce()
    {
        audioSource.clip = bounce;
        audioSource.Play();
    }
    public void SoundEvilLaugh()
    {
        audioSource.clip = evilLaugh;
        audioSource.Play();
    }
    public void SoundOuch()
    {
        audioSource.clip = ouch;
        audioSource.Play();
    }
    public void SoundPowerUp()
    {
        audioSource.clip = powerUp;
        audioSource.Play();
    }
    public void SoundPop()
    {
        audioSource.clip = pop;
        audioSource.Play();
    }
    public void SoundWoohoo()
    {
        audioSource.clip = woohoo;
        audioSource.Play();
    }
    public void SoundGoreSplat()
    {
        audioSource.clip = goreSplat;
        audioSource.Play();
    }
    public void SoundOrcYell()
    {
        audioSource.clip = orcYell;
        audioSource.Play();
    }
    public void SoundSkull()
    {
        audioSource.clip = skullSound;
        audioSource.Play();
    }
}