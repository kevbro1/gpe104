﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Death volumes specific to the spikes
public class DeathSpikes : MonoBehaviour
{
    private PlayerPawn playerPawn; // Player pawn component
    private SoundManager sm; // Sound manager component

    // Start is called before the first frame update
    void Start()
    {
        playerPawn = GameManager.instance.playerObject.GetComponent<PlayerPawn>(); // Set player pawn variable
        sm = GameObject.FindWithTag("MainCamera").GetComponent<SoundManager>(); // Set sound manager variable
    }

    // Check if the player collides with the death spikes
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player")) // Check that the other object is the player
        {
            playerPawn.Death(); // Run the player death method
            sm.SoundGoreSplat(); // Play the splat sound
        }
    }
}
