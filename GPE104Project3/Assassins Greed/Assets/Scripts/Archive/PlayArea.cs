﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Sets play area boundaries and destroys GameObjects that exit the boundaries
// Player is the exception and is reset to center and one life deducted
public class PlayArea : MonoBehaviour
{
    private GameObject player; // The player game Object
    private Transform p; // Transform component for player
    private void Start()
    {
        player = GameObject.FindWithTag("Player"); // Set player to GameObject with "player" tag (Must tag the player in Unity)
        p = player.GetComponent<Transform>(); // Transform component for player
    }
    // Collider function
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject != player) // Check that other.Gameobject is not the player
        {
            Destroy(other.gameObject); // If not the player, then destroy the gameObject
        }
        else // If it is the player than deduct lives and reset to center position
        {
            //if (GameManager.instance.lives > 0)
            //{
            //    GameManager.instance.lives--; // Subtract one life
            //    Vector3 myVector = new Vector3(0, 0, 0); // Resets player to center of play area
            //    p.position = myVector; // Player position set to myVector  
            //}
        }
    }
}
