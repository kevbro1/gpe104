﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Game Manager singleton. Keeps static variables and Game Finite States and methods
public class GameManager : MonoBehaviour
{
    public static GameManager instance; // Game Manager static instance
    public GameObject playerObject; // Player object
    public GameObject playerPrefab; // playerPrefab gameObject
    public GameObject gameManager; // GameManager
    public GameObject pControllerObject; // Player Controller separate from player
    public GameObject pauseMenu; // Pause menu game object
    public SoundManager soundManager; // Sound manager attached to main camera
    public Transform playerSpawn; // Player spawn transform component
    public GameObject checkpointCanvas; // Checkpoint display game object for the checkpoint notification

    // Lists
    public List<GameObject> enemiesList; // List of enemies and spawns
    public List<Transform> enemySpawnList;
    public List<GameObject> pickupList; // List of pickups and spawns
    public List<Transform> pickupSpawnList;
    public List<GameObject> DeathVolumes; // List of death volumes
    public List<Transform> playerCheckpoints; // List of player checkpoints
       
    // Other variables
    [HideInInspector] public int playerLives; // Player's current life count
    [HideInInspector] public int pickupCount = 0; // Counts the number of pickups spawned
    [HideInInspector] public int fireballCount; // Counts current number of fireballs the player has remaining
    [HideInInspector] public int enemyCount = 0; // Counts the number of enemies spawned
    [HideInInspector] public string gameState = "active";
    public int startLives = 3; // Starting lives of the player
    public int checkpointIndex; // Current checkpoint in the list
    public int startingCheckpoint = 0; // Set the starting checkpoint
    public int maxEnemies = 10; // Maximum number of enemies spawned
    public int maxPickups = 5; // Maximum number of pickups spawned
    public int fireballMax = 5; // Maximum number of fireballs the player can have
    
    // Create a singleton of the Game Manager
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject); // Destroy if duplicated
        }
        
    }
    // Set variables at the start of the game
    void Start()
    {
        playerLives = startLives; // Sets player lives to starting lives
        fireballCount = fireballMax; // Sets fireball count to max fireballs
        checkpointIndex = startingCheckpoint; // Sets checkpoint to the starting checkpoint
        playerSpawn = playerCheckpoints[checkpointIndex]; // Player spawn set to the current index in the checkpoint list
        playerObject = Instantiate(playerPrefab, playerSpawn.position, playerSpawn.rotation) as GameObject; // Create an instance of the player
        pControllerObject = GameObject.FindWithTag("GameController"); // Find PlayerController object
        soundManager = GameObject.FindWithTag("MainCamera").GetComponent<SoundManager>(); // Sound Manager component attached to main camera
        pauseMenu = GameObject.FindWithTag("PauseMenu"); // Find PauseMenu object
        pauseMenu.SetActive(false); // Sets pauseMenu to inactive at start
        checkpointCanvas = GameObject.FindWithTag("CheckpointCanvas"); // Find checkpoint canvas object
        checkpointCanvas.SetActive(false); // Sets checkpoint canvas to inactive at start
    }

    void Update()
    {
        // Game Finite State Machine

        // Game is active (being played)
        if (gameState == "active")
        {
            GameActive();
            if (Input.GetKey(KeyCode.Escape))
            {
                gameState = "pause";
            }
        }
        // Game is paused
        if (gameState == "pause")
        {
            GamePause();
        }
        // Game in main menu
        if (gameState == "menu")
        {
            gameState = "active";
            MainMenu();
        }
        // Quit Game
        if (gameState == "quit")
        {
            QuitGame();
        }
        // Defeat state
        if (gameState == "defeat")
        {
            Defeat();
        }
        // Victory state
        if (gameState == "victory")
        {
            Victory();
        }
    }

    // Finite State methods
    public void GameActive()
    {
        // Disable pause menu
        pauseMenu.SetActive(false);

        // Enable all AI movement
        Time.timeScale = 1;

        // Enable player controller
        pControllerObject.GetComponent<PlayerController>().enabled = true;
    }
    public void GamePause()
    {
        // Enable pause menu
        pauseMenu.SetActive(true);

        // Enable all AI movement
        Time.timeScale = 0;

        // Enable player controller
        pControllerObject.GetComponent<PlayerController>().enabled = false;
    }
    public void MainMenu()
    { 
        SceneManager.LoadScene(0); // Load Main Menu
    }
    public void QuitGame()
    {
        Application.Quit(); // Quit Game
    }
    public void Defeat()
    {
        SceneManager.LoadScene(3); // Load Defeat Screen
    }
    public void Victory()
    {
        SceneManager.LoadScene(2); // Load Victory Screen
    }
}
