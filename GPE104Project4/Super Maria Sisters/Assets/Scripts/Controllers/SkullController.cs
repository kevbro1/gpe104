﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Includes Skull Finite States
public class SkullController : AIController
{
    [HideInInspector] public SkullPawn skullPawn;
    void Start()
    {
        aiDetection = GetComponent<AIDetection>(); // AI Detection component
        skullPawn = GetComponent<SkullPawn>(); // Skull pawn
    }

    void Update()
    {
        // AI Finite State Machine
        if (aiState == "patrolling")
        {
            DoPatrol();
            // Transition to Attacking
            if (aiDetection.PlayerDetected(GameManager.instance.playerObject) == true) // Check that player is detected
            {
                aiState = "attacking";
            }
        }
        if (aiState == "attacking")
        {
            DoAttack();
            // Transition to patrolling
            if (aiDetection.PlayerDetected(GameManager.instance.playerObject) == false) // Check that player is not detected
            {
                aiState = "patrolling";
            }
        }
        if (aiState == "dead")
        {
            // Call method when skull dies
            DoDead();
        }
    }
    public override void DoAttack()
    {
        skullPawn.HeatSeekAttack(); // Method for skull heat seeking attack
    }
}
