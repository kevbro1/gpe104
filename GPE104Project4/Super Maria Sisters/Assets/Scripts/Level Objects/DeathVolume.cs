﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Generic death volume script
public class DeathVolume : MonoBehaviour
{
    private PlayerPawn playerPawn; // Player pawn
    private SoundManager sm; // sound manager

    // Start is called before the first frame update
    void Start()
    {
        playerPawn = GameManager.instance.playerObject.GetComponent<PlayerPawn>(); // Set player pawn
        sm = GameObject.FindWithTag("MainCamera").GetComponent<SoundManager>(); // Set sound manager
    }
    // Check that the player collides with the death volume
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player")) // Ensure the other object is the player
        {
            sm.SoundSplash(); // Death splash sound used when the player drowns
            playerPawn.Death(); // Run the player death method
        }
    }   
}
