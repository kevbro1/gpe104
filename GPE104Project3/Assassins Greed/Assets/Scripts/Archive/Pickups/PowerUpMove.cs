﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Moves Power ups random inside circle on a timer
public class PowerUpMove : MonoBehaviour
{
    private Transform tf; // Transform component
    private Vector3 rVector; // Random Vector3
    public float moveTimer = 3; // Move Timer to determine how frequently the power up will move
    public int vectorMultiplier = 8; // Vector multiplier, greater number = larger circle
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // Transform Component
        StartCoroutine("MovePowerUpEvent"); // Begin coroutine for MovePowerUpEvent
    }
    private IEnumerator MovePowerUpEvent()
    {
        while (true)
        {
            yield return new WaitForSeconds(moveTimer); // Timer
            rVector = Random.insideUnitCircle; // Random vector inside circle
            rVector *= vectorMultiplier; // Multiplied by 5
            Vector3 powerUpMove = tf.position + rVector; // Create Vector3 combining transform and rVector
            tf.position = powerUpMove; // Move power up to new position
        }
    }
}