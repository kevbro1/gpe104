﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script used to spawn random enemies at random spawn points
public class EnemySpawn : MonoBehaviour
{
    private List<Transform> spawnList; // List of all spawn points, set list in Unity
    private List<GameObject> enemyList; // List of enemies, set list in Unity
    private GameObject randomEnemy; // Random enemy object
    private Transform spawnPoint; // Transform component for spawnPoint
    public float spawnSpeed = 2; // Allows setting of Spawn Speed in Unity

    void Start()
    {
        spawnList = GameManager.instance.enemySpawnList; // The spawnList is taken from the GameManager.
        enemyList = GameManager.instance.enemiesList; // The enemyList is taken from the GameManager.
        StartCoroutine("SpawnEnemyEvent"); // Begin coroutine for SpawnEnemyEvent
    }

    // Coroutine to spawn enemies on a timer
    private IEnumerator SpawnEnemyEvent()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnSpeed); // Timer
            // Check that enemyCount does not exceed maxEnemies
            if (GameManager.instance.maxEnemies > GameManager.instance.enemyCount)
            {
                spawnPoint = spawnList[Random.Range(0, spawnList.Count)]; // Random spawnPoint from list
                randomEnemy = enemyList[Random.Range(0, enemyList.Count)]; // Random enemy from list
                GameManager.instance.enemySpawnList.Remove(spawnPoint); // Remove spawn point from list when used
                // Create enemyClone instance
                GameObject enemyClone = Instantiate(randomEnemy, spawnPoint.position, spawnPoint.rotation) as GameObject;
                GameManager.instance.enemyCount++; // Increase enemyCount by one
            }
        }
    }
}
