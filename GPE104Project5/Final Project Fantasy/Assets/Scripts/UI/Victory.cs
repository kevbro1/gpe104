﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Victory screen script
public class Victory : MonoBehaviour
{
    public void ContinueGame()
    {
        GameManager.instance.waveManager.waveState = "postWave"; // Set game to active to resume
    }
    public void MainMenu()
    {
        GameManager.instance.gameState = "menu"; // Set game state to main menu to return to the menu
    }
    public void QuitGame()
    {
        GameManager.instance.gameState = "quit"; // set game state to quit to end the game
    }
}
