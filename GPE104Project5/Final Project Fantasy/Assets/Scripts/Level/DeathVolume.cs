﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Damages the player, used in the fire walls to prevent the player from going into enemy spawn.
public class DeathVolume : MonoBehaviour
{
    public float damageAmount = 50; // Damage dealt by death volume
    public bool canDamage = true; // Used for coroutine timer to delay damage
    public float damageDelay = 3.0f; // Damage delay time
    private void OnCollisionEnter2D(Collision2D other)
    {
        // If the other object is the player deal damage and set canDamage to false
        if (other.gameObject == GameManager.instance.player && canDamage == true)
        {
            GameManager.instance.playerData.currentHealth -= damageAmount;
            canDamage = false;
            StartCoroutine("DamageDelay");
        }
    }
    // Coroutine to delay damage after the collider is hit. It gives the player time to move away
    private IEnumerator DamageDelay()
    {
        while (true)
        {
            yield return new WaitForSeconds(damageDelay);
            canDamage = true;
        }
    }
}
